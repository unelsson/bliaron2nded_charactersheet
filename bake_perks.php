<?php
// Bliaron Character Sheet - Bakes js files based on perks saved as csv.
// LGPL-3.0-or-later

// open all csv files
// filename --> object name in js file

// Covert to javascript or json objects

// Save to file

// Display a message to user
$out = "// Pre-baked information about perks, spell effects and such. Based on csv/perks.csv etc. baked by running bake_perks.php" .PHP_EOL .PHP_EOL;
foreach (glob("csv/*.csv") as $filename) {
	$title = basename($filename,".csv");

	echo "<h2>$title: $filename</h2>".PHP_EOL;

	$csv = array_map('str_getcsv', file($filename));

	array_walk($csv, function(&$a) use ($csv) {
		$a = array_combine($csv[0], $a);

	});
	// array_shift($csv); # remove column header

	// var_dump($csv);
	$out .= 'var '. $title.' = {'.PHP_EOL;
	print('<table>');
	print('<tr>');
	// print('<th>product_code</th>');
	foreach($csv[0] as $field){
		print('<th>'.$field.'</th>');

	}
	print('</tr>');

	array_shift($csv); # remove column header
	$outlines = array();
	foreach($csv as $lineno => $line){
		print('<tr>');
		$outl = array();
		foreach($line as $key => $content){
			print('<td>'.$content.'</td>');
			$outl[] = '"'.$key.'": "'.$content.'"';
		}
		$outlines[] =  '	"'.$line['name'].'": {'.implode(', ', $outl).', "store_id": '.$lineno.'}';
		print('</tr>');

	}
	$out .= implode(','.PHP_EOL, $outlines).PHP_EOL;
	print('</table>');


	// $out .= json_encode($csv).PHP_EOL;


	$out .= '};'.PHP_EOL .PHP_EOL .PHP_EOL;
}
file_put_contents("csv.js", $out);
?>
