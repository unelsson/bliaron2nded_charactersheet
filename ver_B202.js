/*bliaron_hahmolomake lomake model*/
// LGPL-3.0-or-later

var lomake = {
	versio: "B202",
	playerinformation: {
		type: "editable",
		store: "text", // Method for storing data in the URL hash
		id : "1", // single character id for this area, necessary for storing data in URL because javascirpt doesn't guarantee object property order
		slots: ["Player name", "Character name","Concept","Age","Clan or homeland"]
	},
	attributes: {
		title: "Attributes",
		type: "text&number",
		id : "2",
		store: "special",
		cost: [2,4,6,8,10,12,14,16,18,20], // Cost per level, or array, for cost per on each lvl
		special_cost: [1,1,1,1,2],
		creation_vals:[0,0,0,0,0,0,0], // Keeps track of the values given at character creation time, or how many special points were used before exp points are needed
		slots: ["Strength / Destruction","Dexterity / Kinetic","Stamina / Life","Charisma / Heat","Intelligence / Thought","Willpower / Matter","Senses / Senses"],
		special_cost_pool: 18, // Extra character creation points
		special_cost_used: 0,
		pools: [0,0,0,0,0,0,0] // Spendable and replenishable points
	},
	magicskills: {
		title: "Magic skills",
		type: "text&number",
		id : "3",
		cost: [1,2,3,4,5,6,7,8,9,10],
		slots: ["Spellcasting","Runesmithing","Ritualism"]
	},
	perks: {
		title: "Perks",
		type: "select",
		store: "text",
		id : "4",
		select_cost: true,
		slots: [1,2,3,4,5,6,7,8], // slot names are not shown for this type, but need identifiers anyway i guess
		selectables: perks // .explanation also used as tooltips
	},
	effects: {
		title: "Spell Effects",
		type: "select",
		store: "text",
		id : "9",
		per_cost: 5,
		slots: [1,2,3,4,5,6,7,8], // slot names are not shown for this type, but need identifiers anyway i guess
		selectables: effects
	},
	qualities: {
		title: "Spell Qualities",
		type: "select",
		store: "text",
		id : "d",
		per_cost: 5,
		slots: [1,2,3,4,5,6,7,8], // slot names are not shown for this type, but need identifiers anyway i guess
		selectables: qualities
	},
	skills: {
		// Levels 1-2: 1
		// Levels 3-4: 3
		// Levels 5  : 5
		// Levels 6-8: 6
		title: "Skills",
		type: "text&number",
		id : "5",
		cost: [1,2,3,4,5,6,7,8,9,10],
		slots: ["Animal handling","Archery","Athletics","Crafts","Intimidation","Lore","Medicine","Melee","Notice","Seafaring","Seduction","Sleight of Hand","Stealth","Survival","Unarmed"],
		tooltips: skills
	},
	additionals: {
        // Additional stats facilitate role-playing
        // Default value for Wealth = 2
        // Status (in development, may not be needed in character sheet)
        // Reputation (in development, may not be needed in character sheet)
        // Only Wealth is bound to a game mechanic
		title: "Additional stats",
		type: "text&number",
		id : "7",
		can_buy: true, // Player can buy extra points, vals then represents the total of calculated+bought
		cost: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20],
		// calc: Write valid javascript. attr() gets value for an attribute, Math.floor, .ceil & .round can be used too
		// calc: [
		//	"Math.round((attr('Ketteryys / Liike')+attr('Aistit / Aistit'))/2)",
		//	"Math.round((attr('Kunto / El&auml;m&auml;')+attr('Tahdonvoima / Materia'))/2)"],
		slots: ["Wealth","Status","Reputation"],
		calculated: [] // Calculated values
	},
	hitpoints:{
		title: "Hitpoints",
		type: "grid",
		id : "8",
		slots: ["hp"],
		calc: ["attr('Stamina / Life')*5+10"],
		calculated: []
	},
	armor:{
		title: "Armor",
		type: "editable text&number",
		store: "both",
		id : "a",
		slots: ["",""]
	},
	weapons:{
		title: "Weapons",
		type: "editable text&number",
		store: "both",
		id : "b",
		slots: ["","",""]
	},
	exp:{
		title: "Experience points",
		store: "text",
		type: "exp",
		id : "c",
		slots: ["Character Creation","Gained"]
	}

};

/*
function parseURL(){
	// Version B202
	// Parse URL into data structure. lomake should already have .vals[].
	if (location.hash.length > 1){
	var data=location.hash.split("-");
	// Check version
	// console.log(data[0]);
	if (data[0].replace("#","") != lomake.versio){
		alert("The character sheet data stored in the link hash does not match the current version number of the character sheet; Some of your data may be corrupted!");
	}

	for (var i=1; i < data.length; i++){
		var id = data[i][0];
		console.log("Data goes to id: "+id);
		data [i] = data[i].slice(1);
		// What box does the id belong to?
		for (var propt in lomake){
			if (lomake[propt].id == id){
				// Get store method, decide how to parse
				if (lomake[propt].store == "text"){
					var c = data[i].split("_");
					for (var j=0; j < c.length; j++){
						// console.log();
						lomake[propt].vals[j] = atou(c[j]);
					}
				} else if (lomake[propt].store == "both"){
					var c = data[i].split("_");
					for (var j=0; j < c.length; j++){
						// console.log();
						var d = c[j].split(".");
						lomake[propt].slots[j] = atou(d[0]);
						if (alph.includes(d[1])){
							lomake[propt].vals[j] = parseInt(alph.indexOf(d[1])+10);
						} else {
							lomake[propt].vals[j] = parseInt(d[1]);
						}
					}
				} else if (lomake[propt].store == "special") {
					var c = data[i].split(".");
					var d = c[0].split("");

					for (var j=0; j < d.length; j++){
						if (alph.includes(d[j])){
							lomake[propt].creation_vals[j] = parseInt(alph.indexOf(d[j])+10);
						} else {
							lomake[propt].creation_vals[j] = parseInt(d[j]);
						}
					}
					d = c[1].split("");
					for (var j=0; j < d.length; j++){
						if (alph.includes(d[j])){
							lomake[propt].vals[j] = parseInt(alph.indexOf(d[j])+10);
						} else {
							lomake[propt].vals[j] = parseInt(d[j]);
						}
					}
					if (lomake[propt].pools && c[2]){
						d = c[2].split("");
						for (var j=0; j < d.length; j++){
							if (alph.includes(d[j])){
								lomake[propt].pools[j] = parseInt(alph.indexOf(d[j])+10);
							} else {
								lomake[propt].pools[j] = parseInt(d[j]);
							}
						}
					}

				} else {
					var c = data[i].split("");
					// console.log("else: "+c);
					for (var j=0; j < c.length; j++){
						// console.log(j+" "+c[j]);
						if (alph.includes(c[j])){
							// console.log("afglo to put "+c[j]);
							lomake[propt].vals[j] = parseInt(alph.indexOf(c[j])+10);
						} else {
							// console.log("Trying to put "+c[j]);
							lomake[propt].vals[j] = parseInt(c[j]);
						}
					}
				}
			}
		}
	}
	} else { // Empty hash, initialize some values
		lomake.exp.vals[0] = 50;
	}
} */
