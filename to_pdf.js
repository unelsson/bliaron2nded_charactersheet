const { PDFDocument, rgb, StandardFonts } = PDFLib

const arraySections = new Set(['effectsandqualities', 'perks', 'equipment'])
function getCharacterData(lomake) {
  const result = {}
  for (let k in lomake) {
    const values = lomake[k].vals
    if (arraySections.has(k)) {
      result[k] = values
      continue
    }
    const section = {}
    const slots = lomake[k].slots
    for (let i in slots)
      section[slots[i]] = values[i]
    result[k] = section
  }
  return result
}

const fontSize = 13
const smallRow = 17
const leftLongColumn = 58
const rightLongColumn = 308

function drawText(page, x, y, value) {
  page.drawText(value, {
    size: fontSize,
    x, y,
  })
}

function drawRows(page, x, y, rowHeight, values) {
  for (let i = 0; i < values.length; i++) {
    drawText(page, x, y - rowHeight * i, '' + values[i])
  }
}

function splitIntoColumns(list, count = 2) {
  const columns = []
  for (let i = 0; i < count; i++) columns[i] = []
  list.forEach((it, i) => {
    columns[i % count].push(it)
  })
  return columns
}

const infoOrder = ['Name', 'Age', 'Concept', 'Culture', 'Religion']
function drawInfo(page, playerInfo) {
  drawRows(page, 315, 784, smallRow, infoOrder.map(it => playerInfo[it]))
  drawText(page, 103, 718, playerInfo.Chronicle)
}

const attributeOrder = ["Str (Dest)", "Dex (Kine)", "Sta (Life)", "Cha (Heat)", "Int (Thou)", "Wil (Matt)", "Sen (Sens)"]
function drawAttributes(page, attributes) {
  drawRows(page, 175.5, 643, 25.5, attributeOrder.map(it => attributes[it]))
}

const magicSkillsOrder = ['Spellcasting', 'Runesmithing', 'Ritualism']
function drawMagicSkils(page, magicSkills) {
  drawRows(page, 344.5, 648, smallRow, magicSkillsOrder.map(it => magicSkills[it]));
}

const skillOrder = ["Animal handling",
  "Athletics",
  "Crafts",
  "Intimidation",
  "Lore",
  "Medicine",
  "Melee",
  "Negotiation",
  "Notice",
  "Ranged",
  "Seafaring",
  "Seduction",
  "Sleight of Hand",
  "Stealth",
  "Survival",
  "Unarmed"]
function drawSkills(page, skills) {
  drawRows(page, 526, 648, smallRow, skillOrder.map(it => skills[it]));
}

const additionalsOrder = ['Harmony', 'Reputation', 'Wealth']
function drawAdditionals(page, additionals) {
  drawRows(page, 140.5, 444, smallRow, additionalsOrder.map(it => additionals[it]));
}

function drawExp(page, exp) {
  drawText(page, 190, 427, '' + (exp["Character Creation"] + exp.Gained))
}

function drawEffectsAndQualities(page, effectsAndQualities) {
  const columns = splitIntoColumns(effectsAndQualities);
  const height = 358
  drawRows(page, leftLongColumn, height, smallRow, columns[0].filter(n => n));
  drawRows(page, rightLongColumn, height, smallRow, columns[1].filter(n => n));
}

function drawPerks(page, perks) {
  const columns = splitIntoColumns(perks);
  const height = 240
  drawRows(page, leftLongColumn, height, smallRow, columns[0].filter(n => n));
  drawRows(page, rightLongColumn, height, smallRow, columns[1].filter(n => n));
}

function drawEquipment(page, equipment) {
  drawRows(page, leftLongColumn, 138, smallRow, equipment.filter(n => n))
}

function saveBytes(reportName, bytes) {
  var blob = new Blob([bytes], { type: "application/pdf" })
  var link = document.createElement('a')
  link.href = window.URL.createObjectURL(blob)
  var fileName = reportName
  link.download = fileName
  link.click()
}

async function toPdf() {
  const characterInfo = getCharacterData(lomake)
  const baseSheet = await fetch('./Bliaron_2ed_character_sheet.pdf').then(res => res.arrayBuffer())
  const pdfDoc = await PDFDocument.load(baseSheet)

  const page = pdfDoc.getPages()[0]
  drawInfo(page, characterInfo.playerinformation)
  drawAttributes(page, characterInfo.attributes)
  drawMagicSkils(page, characterInfo.magicskills)
  drawSkills(page, characterInfo.skills)
  drawAdditionals(page, characterInfo.additionals)
  drawExp(page, characterInfo.exp)
  drawEffectsAndQualities(page, characterInfo.effectsandqualities)
  drawPerks(page, characterInfo.perks)
  drawEquipment(page, characterInfo.equipment)

  const name = (characterInfo.playerinformation.Name || 'Character').replaceAll(' ', '_')
  saveBytes(`Bliaron_${name}.pdf`, await pdfDoc.save())
}

window.addEventListener('load', () => {
  document.getElementById('to_pdf').addEventListener('click', toPdf)
})
