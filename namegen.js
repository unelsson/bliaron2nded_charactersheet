// Bliaron character name generator
// LGPL-3.0-or-later

// () makes contained rule letter optional (included 50% of times), but only one letter at a time
var Bliaron_names = [
	"SV(E)SV(E)",
	"SV(n)SV(E)",
	"SV(E)SV(n)",
	"SV(E)SV(N)",
	"SVnV(n)",
	"CVE",
	"CVS",
	"CV(N)",
	"CVn",
	"CVE(N)"
];
var Bliaron_surnames = [
	"CV(E)(N)SV(n)",
	"CV(E)SV(n)",
	"CV(E)SV(E)(N)",
	"CV(E)(N)",
	"CV(E)SV(n)",
	"SVN",
	"SVS"
];

var Bliaron_noble_surnames = [
	"SV(E)(N)SV(n)",
	"SV(E)SV(n)",
	"SV(E)SV(E)(N)",
	"SV(E)N",
	"SV(E)SV(n)",

];

// "a(a/i/u)" == a can be optionally followed by nothing, a, i or u, with equal likelihood
var rules = {
	"C" : ["b(l/r)", "ch", "d(r)", "f(r)", "g(r)", "h", "j", "k(r)", "l", "m", "n", "p(r)", "q", "r", "s(t)", "t(r/l/h)", "v", "w", "z", "b","d","f","g","k","p","t","y"],
	"S" : ["b", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "r", "s", "t", "v", "w"],
	"E" : ["ng", "th", "b", "k", "d", "g", "k", "l", "m", "n", "p", "k", "r", "s", "t", "v", "x"],
	"V" : ["a","e","i","o","u","a(a/i/u)", "e(i)", "i(e/a/u/o)", "o(i/u)", "u(i/u/o)"],
	"N" : ["tan", "gol", "bul", "ol", "dur", "tun","ain"],
	"n" : ["n", "ng", "m", "l", "r"],
	"o" : ["na-", "o'", "el-"]
};


function make_name(template) {
	// Create one name based on template
	var template = template[rand_int(0,template.length-1)];
	var output = "";
	var capitalize_next = true;

	var levels = 0; // How many levels of brackets have been encountered. if in a level, 50-50 chance of outputting the contents of this level
	for (var i=0; i<template.length; i++){
		// Look at template one char at time
		if (template[i] == "("){
			// Next character may be left out
			levels ++;

		} else if (template[i] == ")"){
			levels --;
		} else if (rules[template[i]]){
			// if current character in the template can be found in rules, randomize a character to output
			// C --> "b(l/r)"
			// TODO: randomize and check if this level already has been outputted
			// levels = difficulty level of being included
			var yes = rand_int(0, 4);
			// console.log(yes);
			if (yes >= levels){
				var rule = rules[template[i]];
				var chosen = rule[rand_int(0,rule.length-1)];
				var string = parserule(chosen);

				if (capitalize_next) string = string.charAt(0).toUpperCase() + string.slice(1);
				capitalize_next = false;
				output += string;
			}


		} else if (template[i] == " "){
			output += " ";
			capitalize_next = true;
		}
		// console.log(output);
	}
	return output;
}
function parserule(rl) {
	// parse "a(a/i/u)" into possibilities and output a string
	var output ="";
	var levels = 0;
	var gathered = [""];
	var possible = false;
	for (var i=0; i<rl.length; i++){
		if (rl[i] == "("){
			// maybe output one of the characters that follow "("
			levels ++;
			i++;
			gathered.push(rl[i]);
		} else if (rl[i] == ")"){
			levels --;
			// Randomly output one of gathered conditional characters
			output += gathered[rand_int(0,gathered.length-1)];
			gathered = [""];
		} else if (rl[i] == "/" && levels > 0){
			// next character is a possibility
			i++;
			gathered.push(rl[i]);
		} else if (levels == 0){
			output += rl[i];
		}
		// console.log(output, gathered);
	}

	return output;
}

// function rand_int(min, max) {
    // return Math.floor(Math.random()*(max-min+1)+min);
// }

function consolenames(num) {
	// output num names into console
	for (var i=0; i<num; i++){
		console.log(make_name(Bliaron_names)+" "+make_name(Bliaron_surnames));
	}

}

function get_names(num) {
	// output num names into console
	var output = [];
	for (var i=0; i<num; i++){
		output.push(make_name(Bliaron_names)+" "+make_name(Bliaron_surnames));
	}
	return output;
}

// window.onload = function() {
	// consolenames(10);
// }
