/*bliaron_hahmolomake lomake model*/
// LGPL-3.0-or-later

var lomake = {
	versio: "B204",
	playerinformation: {
		type: "editable",
		store: "text", // Method for storing data in the URL hash
		id : "1", // single character id for this area, necessary for storing data in URL because javascirpt doesn't guarantee object property order
		slots: ["Player name", "Character name","Chronicle","Concept","Age","Culture","Religion","Motivation"]
	},
	attributes: {
		title: "Attributes",
		type: "text&number",
		id : "2",
		store: "special",
		cost: [2,4,6,8,10,12,14,16,18,20], // Cost per level, or array, for cost per on each lvl
		special_cost: [1,1,1,1,2],
		creation_vals:[0,0,0,0,0,0,0], // Keeps track of the values given at character creation time, or how many special points were used before exp points are needed
		slots: ["Strength / Destruction","Dexterity / Kinetic","Stamina / Life","Charisma / Heat","Intelligence / Thought","Willpower / Matter","Senses / Senses"],
		special_cost_pool: 18, // Extra character creation points
		special_cost_used: 0,
		pools: [0,0,0,0,0,0,0] // Spendable and replenishable points
	},
	magicskills: {
		title: "Magic skills",
		type: "text&number",
		id : "3",
		cost: [1,2,3,4,5,6,7,8,9,10],
		slots: ["Spellcasting","Runesmithing","Ritualism"]
	},
	perks: {
		title: "Perks",
		type: "select",
		store: "store_id", // store_id exists in selectables, no need to store whole text
		id : "4",
		select_cost: true,
		slots: [1,2,3,4,5,6,7,8], // slot names are not shown for this type, but need identifiers anyway i guess
		selectables: perks // .explanation also used as tooltips
	},
	effects: {
		title: "Spell Effects",
		type: "select&number", // will have .vals and .levels
		store: "text&level",
		id : "9",
		per_level_cost: 1,
		slots: [1,2,3,4,5,6,7,8], // slot names are not shown for this type, but need identifiers anyway i guess
		selectables: effects
	},
	qualities: {
		title: "Spell Qualities",
		type: "select&number",
		store: "text&level",
		id : "d",
		per_level_cost: 1,
		slots: [1,2,3,4,5,6,7,8], // slot names are not shown for this type, but need identifiers anyway i guess
		selectables: qualities
	},
	skills: {
		// Levels 1-2: 1
		// Levels 3-4: 3
		// Levels 5  : 5
		// Levels 6-8: 6
		title: "Skills",
		type: "text&number",
		id : "5",
		cost: [1,2,3,4,5,6,7,8,9,10],
		slots: ["Animal handling","Archery","Athletics","Crafts","Intimidation","Lore","Medicine","Melee","Negotiation","Notice","Seafaring","Seduction","Sleight of Hand","Stealth","Survival","Unarmed"],
		tooltips: skills
	},
	additionals: {
        // Additional stats facilitate role-playing
        // Default value for Wealth = 2
        // Status (in development, may not be needed in character sheet)
        // Reputation (in development, may not be needed in character sheet)
        // Only Wealth is bound to a game mechanic
		title: "Additional stats",
		type: "text&number",
		id : "7",
		can_buy: true, // Player can buy extra points, vals then represents the total of calculated+bought
		cost: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
		// calc: Write valid javascript. attr() gets value for an attribute, Math.floor, .ceil & .round can be used too
		// calc: [
		//	"Math.round((attr('Ketteryys / Liike')+attr('Aistit / Aistit'))/2)",
		//	"Math.round((attr('Kunto / El&auml;m&auml;')+attr('Tahdonvoima / Materia'))/2)"],
		slots: ["Wealth"],
		calculated: [] // Calculated values
	},
	hitpoints:{
		title: "Hitpoints",
		type: "grid",
		id : "8",
		slots: ["hp"],
		calc: ["attr('Stamina / Life')*5+10"],
		calculated: []
	},
	armor:{
		title: "Armor",
		type: "editable text&number",
		store: "both",
		id : "a",
		slots: ["",""]
	},
	weapons:{
		title: "Weapons",
		type: "editable text&number",
		store: "both",
		id : "b",
		slots: ["","",""]
	},
	exp:{
		title: "Experience points",
		store: "large_number",
		type: "exp",
		id : "c",
		give_exp: 65, // How much exp to give new character
		slots: ["Character Creation","Gained"]
	}

};
