// Bliaron random character creator
// LGPL-3.0-or-later

// Should only be called when advancedoptionsbox exists!
function load_advanced_options(target_element) {
    var advanced_options_element = document.getElementById("advancedoptionsbox");

    for (var i = 0; i < advanced_options.length; i++) {
        var option_div = creel("div","","advancedoptionsindiv","","",true);
        var label = creel("label","","advancedoptionsindiv",["for",advanced_options[i].name],"",true);
        label.innerHTML = advanced_options[i].name;
        addel(option_div, label);

        if (advanced_options[i].type == "checkbox") {
            var option_text = "'" + advanced_options[i].name + "'" + ",this.checked";
            var checkbox = creel("input", advanced_options[i].name, "advancedoptionsindiv",
                ["type", "checkbox",
                "onclick", "(set_advanced_option(" + option_text + "));"],
                "", true);
            if (advanced_options[i].value == 1) checkbox.checked = true;
            addel(option_div, checkbox);
        }
        if (advanced_options[i].type == "number") {
            var option_text = "'" + advanced_options[i].name + "'" + ",this.value";
            var inputbox = creel("input", advanced_options[i].name, "advancedoptionsindiv",
                ["type", "number",
                "min", "0",
                "max", advanced_options[i].max,
                "value", advanced_options[i].value,
                "onclick", "(set_advanced_option(" + option_text + "));",
                "onkeydown", "(set_advanced_option(" + option_text + "));"],
                "", true);
            addel(option_div, inputbox);
        }
        addel(target_element, option_div);
    }
}

function set_advanced_option(option_name, option_value) {
    for (var i = 0; i < advanced_options.length; i++) {
        if (advanced_options[i].name == option_name) advanced_options[i].value = option_value;
    }
}

function init_concept_list() {
    var conceptpicker = document.getElementById("conceptpicker");

    var first_element = document.createElement("OPTION");
    first_element.text = "(generate from concept)";
    conceptpicker.options.add(first_element);

    for (var i = 0; i < concepts.length; i++) {
        for (var j = 0; j < concepts[i].length; j++) {
            var concept_element = document.createElement("OPTION");
            concept_element.value = concepts[i][j].name;
            concept_element.text = concepts[i][j].name;
            conceptpicker.options.add(concept_element);
        }
    }
}

function set_concept(concept) {

	if (concept.value != "(generate from concept)") {
		put_value("playerinformation","Concept",concept.value);
		generate_character(concept.value);
		render_lomake();
	}

}

// Fills character sheet with randomized character data
function generate_character(concept) {

    // Parse advanced options
    var npc_mode = 0;
    var option_randomize_attributes = 1;
    var option_handle_related_perks = 1;
    var option_balance_skills = 1;
    var option_randomize_effqua = 1;
    var option_awakened = 1; //ability to have magic skills, might be turned 0 if using npc_mode
    var option_randomize_perks = 1;
    var maxperks = 5;
    for (var i = 0; i < advanced_options.length; i++) {
        if (advanced_options[i].name == "NPC-mode") npc_mode = advanced_options[i].value;
        if (advanced_options[i].name == "Randomize Attributes") option_randomize_attributes = advanced_options[i].value;
        if (advanced_options[i].name == "Concept-based skill randomization") option_handle_related_perks = advanced_options[i].value;
        if (advanced_options[i].name == "Balance exp use with random skills") option_balance_skills = advanced_options[i].value;
        if (advanced_options[i].name == "Randomize Mastered Effects and Qualities") option_randomize_effqua = advanced_options[i].value;
        if (advanced_options[i].name == "Randomize Perks") option_randomize_perks = advanced_options[i].value;
        if (advanced_options[i].name == "Max random perks") maxperks = advanced_options[i].value;
    }

	var creationpoints = parseInt(lomake.exp.vals[0]);

    if (npc_mode == 1) {
        for (var i = 0; i < concepts.length; i++) {
            for (var j = 0; j < concepts[i].length; j++) {
                if (concepts[i][j].name == concept) {
                    creationpoints = concepts[i][j].concept_experience;
                    for (var k = 0; k < concepts[i][j].concept_flags.length; k++) {
                        if (concepts[i][j].concept_flags[k] == "Non-awakened") option_awakened = 0;
                    }
                }
            }
        }
    }

	// Age
	var age = rand_int(8,20) + rand_int(4,20) + rand_int(4,30);
	put_value("playerinformation","Age",age);

	// Family
	// Concept (depends on wealth)

    // Target wealth for character (final wealth is set by perks)
	var wealth = rand_int(0,100);
	if (wealth > 99) wealth = 4;
    else if (wealth > 90) wealth = 3;
    else if (wealth > 75) wealth = 2;
    else if (wealth > 15) wealth = 1;
	else wealth = 0

    // Character name
	try {
		// Requires namegen.js
		var can_afford_surname = rand_int(0,10);
		if (wealth => 4){
			var charactername = make_name(Bliaron_names)+" "+make_name(Bliaron_noble_surnames);
		} else if (wealth > 0 || can_afford_surname > 5){
			var charactername = make_name(Bliaron_names)+" "+make_name(Bliaron_surnames);
		} else {
			var charactername = make_name(Bliaron_names);
		}

	} catch {
		console.log("makename.js is not available, was not able to generate character name");
		var charactername = "Unnamed Character";
	}
	put_value("playerinformation","Name",charactername);

    if (concept == "random") var concept = pick_from_array(concepts[wealth]);
    else for (var i = 0; i < concepts.length; i++) {
        for (var j = 0; j < concepts[i].length; j++) {
            if (concepts[i][j].name == concept) concept = concepts[i][j];
        }
    }

	put_value("playerinformation","Concept",concept.name);

    var culture = randomize_culture(concept);
    var religion = randomize_religion(concept, culture);
    if (option_handle_related_perks == true) handle_relatedperks(concept, culture, religion, true); // Perks inherent to the concept, culture and religion
	if (option_randomize_attributes == true) randomize_attributes(concept.attrs);
    remove_duplicates(concept.skills);
	randomize_favorite_skills(concept.skills, culture.skills, religion.skills, creationpoints, true);
    if (option_awakened == false) {
        // Zero out magic skills even if they are given in randomize_favorite_skills().
        // With zero magic skills, there is zero chance of effects and qualities in randomize_magic().
        for (var i = 0; i < lomake.magicskills.vals.length; i++) {
            lomake.magicskills.vals[i] = 0;
		}
    }
	if (option_randomize_effqua == true) randomize_magic(creationpoints, true); // Mastered Effects and Qualities
	if (option_randomize_perks == true) randomize_perks(concept, culture, religion, false, maxperks); // Random perks, not related to the concept, religion or culture
    if (option_balance_skills == true) balance_creationpoint_use(creationpoints);

    // get final wealth from perks
    wealth = 1;
    for (var i = 0; i < lomake.perks.slots.length; i++) {
        if (lomake.perks.vals[i] != "") {
            var searchperkexplanation = perks[lomake.perks.vals[i]].explanation;
            var searchwealthfromexplanation = searchperkexplanation.search("Wealth");
            if (searchwealthfromexplanation != -1) {
                var perkwealth = parseInt(searchperkexplanation.substr(searchwealthfromexplanation + 7, 1));
                if (perkwealth == 0)
                {
                    wealth = 0;
                    continue
                }
                else if (perkwealth > wealth) wealth = perkwealth;
            }
        }
    }

	put_value("additionals","Wealth",wealth);

	randomize_gear(concept, wealth, true);
	// Beginning of adult life
	if (age > 20){

	}
	// Mid life
	if (age > 30){

	}
	// Old
	if (age > 50){

	}
	//History(concept); // It's easiest to just pass this value
	render_lomake();
	backup();
}

function put_value(field, slot, val) {
	// Put data into target slot
	var i = lomake[field].slots.indexOf(slot);
	// console.log("Trying to put",field,slot,val, "into index",i);
	lomake[field].vals[i] = val;
}

function get_value(field, slot) {
	// Put data into target slot
	var i = lomake[field].slots.indexOf(slot);
	// console.log("Trying to put",field,slot,val, "into index",i);
	return lomake[field].vals[i];
}

function remove_duplicates(ar) {
	if (ar){
		for (var j = 0; j < ar.length - 1; j++) {
			for (var i = j + 1; i < ar.length; i++)
			{
				if (ar[j] != "" && ar[i] == ar[j]) ar[i] = "";
			}
		}
	}
}

function remove_lower_sahen_titles()
{
    var highestsahentitle = 0;
    for (var i=0; i<lomake.perks.vals.length; i++) {
        var thissahentitle = 0;
        if (lomake.perks.vals[i] == "Sahen 1st grade (+2)") thissahentitle = 1;
        if (lomake.perks.vals[i] == "Sahen 2nd grade (+4)") thissahentitle = 2;
        if (lomake.perks.vals[i] == "Sahen 3rd grade (+6)") thissahentitle = 3;
        if (lomake.perks.vals[i] == "Sahen 4th grade (+9)") thissahentitle = 4;
        if (thissahentitle > highestsahentitle) highestsahentitle = thissahentitle;
    }
    remove_duplicates(lomake.perks.vals);
    for (var i=0; i<lomake.perks.vals.length; i++) {
        if (lomake.perks.vals[i] == "Sahen novice (+1)" && highestsahentitle > 0) lomake.perks.vals[i] = "";
        if (lomake.perks.vals[i] == "Sahen 1st grade (+2)" && highestsahentitle > 1) lomake.perks.vals[i] = "";
        if (lomake.perks.vals[i] == "Sahen 2nd grade (+4)" && highestsahentitle > 2) lomake.perks.vals[i] = "";
        if (lomake.perks.vals[i] == "Sahen 3rd grade (+6)" && highestsahentitle > 3) lomake.perks.vals[i] = "";
    }
}

function remove_lower_volar_titles()
{
    var highestvolartitle = 0;
    for (var i = 0; i < lomake.perks.vals.length; i++) {
        if (lomake.perks.vals[i] == "Volar mage (+3)") highestvolartitle = 1;
    }
    remove_duplicates(lomake.perks.vals);
    for (var i = 0; i < lomake.perks.vals.length; i++) {
        if (lomake.perks.vals[i] == "Volar apprentice (+1)" && highestvolartitle > 0) lomake.perks.vals[i] = "";
    }
}

function remove_lower_wealth_titles()
{
    var highestwealthtitle = 1;
    for (var i = 0; i < lomake.perks.vals.length; i++) {
        var thiswealth = 1;
        if (is_perk_of_wealth_two(lomake.perks.vals[i])) thiswealth = 2;
        if (is_perk_of_wealth_three_nosahen(lomake.perks.vals[i])) thiswealth = 3;
        if (is_perk_of_wealth_four_nosahen(lomake.perks.vals[i])) thiswealth = 4;
        if (thiswealth > highestwealthtitle) highestwealthtitle = thiswealth;
    }
    remove_duplicates(lomake.perks.vals);
    for (var i = 0; i < lomake.perks.vals.length; i++) {
        if (is_poverty_perk(lomake.perks.vals[i])) highestwealthtitle = 0;
    }
    for (var i = 0; i < lomake.perks.vals.length; i++) {
        if (highestwealthtitle == 0 && (is_perk_of_wealth_two(lomake.perks.vals[i]) || is_perk_of_wealth_three_nosahen(lomake.perks.vals[i]) ||
            is_perk_of_wealth_four_nosahen(lomake.perks.vals[i]))) lomake.perks.vals[i] = "";
        if (is_perk_of_wealth_two(lomake.perks.vals[i])  && highestwealthtitle > 2) lomake.perks.vals[i] = "";
        if (is_perk_of_wealth_three_nosahen(lomake.perks.vals[i]) && highestwealthtitle > 3) lomake.perks.vals[i] = "";
    }
}

function handle_unlikely_perks(concept, culture, religion) {
    //unlikely perks might have been generated, if they are likely in some other variable (e.g. concept, culture or religion)

    var probability_of_unlikely_concept_perk = 10;
    var probability_of_unlikely_culture_perk = 25;
    var probability_of_unlikely_religion_perk = 25;
    for (var j = 0; j < lomake.perks.vals.length; j++) {
        if (concept.unlikely_perks != null) {
            for (i = 0; i < concept.unlikely_perks.length; i++) {
                if (concept.unlikely_perks[i] ==  lomake.perks.vals[j]) {
                    if (rand_int(1,100) >= probability_of_unlikely_concept_perk) lomake.perks.vals[j] = "";
                }
            }
        }
        if (culture.unlikely_perks != null) {
            for (i = 0; i < culture.unlikely_perks.length; i++) {
                if (culture.unlikely_perks[i] == lomake.perks.vals[j]) {
                    if (rand_int(1,100) >= probability_of_unlikely_culture_perk) lomake.perks.vals[j] = "";
                }
            }
        }
        if (religion.unlikely_perks != null) {
            for (i = 0; i < religion.unlikely_perks.length; i++) {
                if (religion.unlikely_perks[i] == lomake.perks.vals[j]) {
                    if (rand_int(1,100) >= probability_of_unlikely_religion_perk) lomake.perks.vals[j] = "";
                }
            }
        }
    }
}

function is_perk_of_wealth_two(perkname)
{
    // Sahen titles are intentionally left untouched
    if (perkname == "Blackhand hunter (+4)" ||
        perkname == "Well-off (+3)") return true;
    return false;
}

function is_perk_of_wealth_three_nosahen(perkname)
{
    if (perkname == "Landowner (+5)" ||
        perkname == "Blackhand authority (+4)") return true;
    return false;
}

function is_perk_of_wealth_three(perkname)
{
    if (perkname == "Landowner (+5)" ||
        perkname == "Sahen 3rd grade (+6)") return true;
    return false;
}

function is_perk_of_wealth_four_nosahen(perkname)
{
    if (perkname == "Noble (+8)") return true;
    return false;
}

function is_perk_of_wealth_four(perkname)
{
    if (perkname == "Noble (+8)" ||
        perkname == "Sahen 4th grade (+9)") return true;
    return false;
}

function is_grandsahen_perk(perkname)
{
    if (perkname == "Sahen novice (+1)") return true;
    if (perkname == "Sahen 1st grade (+2)") return true;
    if (perkname == "Sahen 2nd grade (+4)") return true;
    if (perkname == "Sahen 3rd grade (+6)") return true;
    if (perkname == "Sahen 4th grade (+9)") return true;
    return false;
}

function is_artan_blackhand_perk(perkname)
{
    if (perkname == "Artan Blackhand (+1)") return true;
    return false;
}

function is_bliwon_blackhand_perk(perkname)
{
    if (perkname == "Bliwon Blackhand (+1)") return true;
    return false;
}

function is_blackhand_perk(perkname)
{
    if (perkname == "Bliwon Blackhand (+1)" ||
        perkname == "Hunted Blackhand (-3)" ||
        perkname == "Marked as Blackhand (-5)" ||
        perkname == "Blackhand authority (+4)" ||
        perkname == "Artan Blackhand (+1)") return true;
    return false;
}

function is_poverty_perk(perkname)
{
    if (perkname == "Beggar (-2)" || perkname == "Slave (-4)") return true;
    return false;
}

function sort_perks()
{
    for (var j = 0; j < lomake.perks.vals.length; j++) {
        for (var i = 0; i < lomake.perks.vals.length - 1; i++) {
            if (lomake.perks.vals[i] == "" && lomake.perks.vals[i + 1] != "") {
                lomake.perks.vals[i] = lomake.perks.vals[i + 1];
                lomake.perks.vals[i + 1] = "";
            }
        }
    }
}

function handle_relatedperks(concept, culture, religion, emptify) {
    if (emptify) {
        for (var i = 0; i < lomake.perks.vals.length; i++) {
            lomake.perks.vals[i] = "";
        }
    }

    var counter = 0;
    if (concept.must_have_perks != null) {
        for (var i = 0; i < concept.must_have_perks.length; i++) {
            if (counter < lomake.perks.vals.length) {
                lomake.perks.vals[counter] = concept.must_have_perks[i];
                counter++;
            }
        }
    }
    if (religion.must_have_perks != null) {
        for (var i = 0; i < religion.must_have_perks.length; i++) {
            if (counter < lomake.perks.vals.length) {
                lomake.perks.vals[counter] = religion.must_have_perks[i];
                counter++;
            }
        }
    }
    if (culture.must_have_perks != null) {
        for (var i = 0; i < culture.must_have_perks.length; i++) {
            if (counter < lomake.perks.vals.length) {
                lomake.perks.vals[counter] = culture.must_have_perks[i];
                counter++;
            }
        }
    }
    for (var i = 0; i < 5; i++) {
        var randomhundred = rand_int(1, 100);
        var pointbalance = lomake.exp.give_exp - calculate_cost();
        if (concept.likely_has_perks != null && randomhundred <= 30 && counter < lomake.perks.vals.length) {
            var perkname = pick_from_array(concept.likely_has_perks);
            if (perkname != "" && pointbalance > perks[perkname].cost) {
                lomake.perks.vals[counter] = perkname;
                counter++;
            }
        }
        if (religion.likely_has_perks != null && randomhundred > 30 && randomhundred <= 35 && counter < lomake.perks.vals.length) {
            var perkname = pick_from_array(religion.likely_has_perks);
            if (perkname != "" && pointbalance > perks[perkname].cost) {
                lomake.perks.vals[counter] = perkname;
                counter++;
            }
        }
        if (culture.likely_has_perks != null && randomhundred > 35 && randomhundred <= 40 && counter < lomake.perks.vals.length) {
            var perkname = pick_from_array(culture.likely_has_perks);
            if (perkname != "" && pointbalance > perks[perkname].cost) {
                lomake.perks.vals[counter] = perkname;
                counter++;
            }
        }
    }

    handle_unlikely_perks(concept, culture, religion);
    remove_lower_wealth_titles();
    remove_lower_sahen_titles(); //Remove duplicates of Sahen titles, only leave one instance of the highest title
    remove_lower_volar_titles();
    sort_perks(); //Remove empty perks lines in middle of the list, might also happen if must_have_perks or likely_has_perks are defined as ""
}

function balance_creationpoint_use(creationpoints) {
    var available = creationpoints - calculate_cost(); //var pointbalance = lomake.exp.give_exp - calculate_cost();
    var skillids_exactly_onetoohigh = [];
    var skillids_exactly_onetoolow = [];
    var skillids_overzero = [];
    var skillids_zero = [];
    var runs = 0;
    var maxruns = creationpoints;

    while (available != 0 && runs < maxruns) {
        skillids_exactly_onetoohigh = [];
        skillids_exactly_onetoolow = [];
        skillids_overzero = [];
        skillids_zero = [];
		for (var i = 0; i < lomake.skills.vals.length; i++) {
            currentvalue = parseInt(lomake.skills.vals[i]);
            if (currentvalue == -available) skillids_exactly_onetoohigh.push(i);
            if (currentvalue == available - 1) skillids_exactly_onetoolow.push(i);
            if (currentvalue == 0) skillids_zero.push(i);
            if (currentvalue > 0) skillids_overzero.push(i);
		}
        // First check if there's an exact match
        if (skillids_exactly_onetoohigh.length > 0) {
            var id = rand_int(0, skillids_exactly_onetoohigh.length);
            var randomskill = skillids_exactly_onetoohigh[id];
            lomake.skills.vals[randomskill] = parseInt(lomake.skills.vals[randomskill]) - 1;
        } else if (skillids_exactly_onetoolow.length > 0) {
            var id = rand_int(0, skillids_exactly_onetoolow.length);
            var randomskill = skillids_exactly_onetoolow[id];
            if (parseInt(lomake.skills.vals[randomskill]) < 10)
                lomake.skills.vals[randomskill] = parseInt(lomake.skills.vals[randomskill]) + 1;
        }
        // Then decrease any value above 0, or increase skills at 0 level
        else if (skillids_overzero.length > 0 && available < 0) {
            var id = rand_int(0, skillids_overzero.length);
            var randomskill = skillids_overzero[id];
            lomake.skills.vals[randomskill] = parseInt(lomake.skills.vals[randomskill]) - 1;
        } else if (skillids_zero.length > 0 && available > 0) {
            var id = rand_int(0, skillids_zero.length);
            var randomskill = skillids_zero[id];
            if (parseInt(lomake.skills.vals[randomskill]) < 10)
                lomake.skills.vals[randomskill] = parseInt(lomake.skills.vals[randomskill]) + 1;
        // Finally if all skills are above 0, and there are points to use, just increase a random skill
        } else if (available > 0) {
            var randomskill = rand_int(0, lomake.skills.vals.length);
            if (parseInt(lomake.skills.vals[randomskill]) < 10)
                lomake.skills.vals[randomskill] = parseInt(lomake.skills.vals[randomskill]) + 1;
        } // Note: If all skills are at zero, and we've used too many points, it's a lost cause.
        available = creationpoints - calculate_cost();
        runs++;
    }
    if (available != 0) console.log ("Error: Failed to balance experience points use. Exp:", creationpoints, " available:", available);
}

function randomize_culture(concept) {
    var random_culture = {name: ""};
    var likely_culture = true;
    if (rand_int(0, 100) < 5) likely_culture = false;
    if (concept.likely_cultures != null && concept.likely_cultures.length > 0 && likely_culture) {
        var culturename = pick_from_array(concept.likely_cultures); //pick a culture given by concept
        for (i = 0; i < cultures.length; i++) {
            if (concept.unlikely_cultures == null || concept.unlikely_cultures.indexOf(culturename) == -1) {
                if (cultures[i].name == culturename) random_culture = cultures[i];
            } else {
                random_culture = pick_from_array(cultures); //reroll once, pick random religion
            }
        }
    } else {
        random_culture = pick_from_array(cultures); //pick random religion
    }
    if (random_culture.name == "") console.log("Error in randomizing culture: ", random_culture);
    put_value("playerinformation","Culture",random_culture.name);
    return random_culture;
}

function randomize_religion(concept, culture) {
    var random_religion = {name: ""};
    var likely_religion = true;
    if (rand_int(0, 100) < 33) likely_religion = false;
    if (culture.religions != null && culture.religions.length > 0 && likely_religion) {
        var religionname = pick_from_array(culture.religions); //pick a religion given by culture
        for (i = 0; i < religions.length; i++) {
            if (concept.unlikely_religions == null || concept.unlikely_religions.indexOf(religionname) == -1) {
                if (religions[i].name == religionname) random_religion = religions[i];
            } else {
                random_religion = pick_from_array(religions); //reroll once, pick random religion
            }
        }
    } else {
        random_religion = pick_from_array(religions); //pick random religion
    }
    if (random_religion.name == "") console.log("Error in randomizing religion: ", random_culture);
    put_value("playerinformation","Religion",random_religion.name);
    return random_religion;
}

function randomize_gear(concept, wealth, emptify) {
	if (emptify){
		for (var i=0; i<lomake.equipment.slots.length; i++) {
			lomake.equipment.vals[i] = "";
		}
	}
	// Gear depends on wealth and fighting skills
	// "Melee", "Ranged"
	var weapnames = Object.getOwnPropertyNames(melee_weapons);
	var archnames = Object.getOwnPropertyNames(archery_weapons);
	var armornames = Object.getOwnPropertyNames(armor);
	var equipment_amount = 0;

    var randommeleeweapons = 1;

    for (i = 0; i < randommeleeweapons; i++) {
        var prevrw = -1;
    	if (lomake.skills.vals[lomake.skills.slots.indexOf("Melee")] > 0) {
    		var factor = lomake.skills.vals[lomake.skills.slots.indexOf("Melee")]+wealth * 2;
    		// var fff = rand_int(0,factor);
    		var rw = rand_int(Math.min(wealth/2, weapnames.length/2), Math.min(factor, weapnames.length - 1));
            var thisweaponname = weapnames[rw] + " (" + melee_weapons[weapnames[rw]].damage + ")";
            if (equipment_amount > 0) {
                for (equipments = 0; equipments < lomake.equipment.vals.length; equipments++) {
                    if (lomake.equipment.vals[equipments] == thisweaponname && rw > 0) {
                        rw = rw - 1;
                        if (rw < 0) rw = 1;
                        //console.log("already has ", thisweaponname, "");
                        thisweaponname = weapnames[rw] + " (" + melee_weapons[weapnames[rw]].damage + ")";
                        //console.log("so offer changed to ", thisweaponname);
                    }
                }
            }


            if (melee_weapons[weapnames[rw]].wealthrequirement <= wealth) {
                //console.log("giving:",thisweaponname);
        		lomake.equipment.vals[equipment_amount] = thisweaponname;
    		    equipment_amount++;
            } else if (rw >= 2) {
                //console.log("can't afford:", thisweaponname);
                rw = rw - 2;
                thisweaponname = weapnames[rw] + " (" + melee_weapons[weapnames[rw]].damage + ")";
                //console.log("giving:", thisweaponname, " instead.");
                lomake.equipment.vals[equipment_amount] = thisweaponname;
                equipment_amount++;
            }
    	}
    }

	if (lomake.skills.vals[lomake.skills.slots.indexOf("Ranged")] > 0) {

		var wep = rand_int(Math.min(wealth/2, archnames.length/2 ), archnames.length - 1);
        //console.log(wep);

        if (archery_weapons[archnames[wep]].wealthrequirement <= wealth) {
		    lomake.equipment.vals[equipment_amount] = archnames[wep] + " (" + archery_weapons[archnames[wep]].damage + ")";
		    equipment_amount++;
        } else {
            lomake.equipment.vals[equipment_amount] = archnames[0] + " (" + archery_weapons[archnames[0]].damage + ")";
            equipment_amount++;
        }
	}

	// Randomize armor
	if (lomake.skills.vals[lomake.skills.slots.indexOf("Melee")] > 2 ||
	    lomake.skills.vals[lomake.skills.slots.indexOf("Ranged")] > 3)
	  {
		var ar = rand_int(Math.min(wealth/2, armornames.length/2), armornames.length - 1);
		if (armor[armornames[ar]].wealthrequirement <= wealth) {
            lomake.equipment.vals[equipment_amount] = armornames[ar] + " (" + armor[armornames[ar]].armor + ")";
		    equipment_amount++;
        } else if (ar > 1) {
            //console.log("can't afford:", armornames[ar]);
            ar = ar - 2;
            lomake.equipment.vals[equipment_amount] = armornames[ar] + " (" + armor[armornames[ar]].armor + ")";
            equipment_amount++;
            //console.log("giving:", armornames[ar], " instead.");
        }
	}

    // Equipment speficic to concept
    if (concept.equipment != null) {
        for (i = 0; i < concept.equipment.length; i++) {
            lomake.equipment.vals[equipment_amount] = concept.equipment[i];
            equipment_amount++;
        }
    }
    if (concept.name == "Farmer" || concept.name == "Peasant") {
        lomake.equipment.vals[equipment_amount] = "Hoe (1)";
        equipment_amount++;
    }
    if (concept.name == "Craftman" ||
        concept.name == "Burglar" ||
        concept.name == "Artist" ||
        concept.name == "Burglar" ||
        concept.name == "Poet" ||
        concept.name == "Smith" ||
        concept.name == "Spy" ||
        concept.name == "Sahen Courier" ||
        concept.name == "Fisherman" ||
        concept.name == "Kalthan researcher") {
        if ( wealth > 0 && wealth <= 3) {
            lomake.equipment.vals[equipment_amount] = "Specialty trade-tools (+1)"
            equipment_amount++;
        } else if (wealth > 3) {
            lomake.equipment.vals[equipment_amount] = "Unique trade-tools (+2)"
            equipment_amount++;
        }
    }

    // Equipment given by perks
    for (var i = 0; i < lomake.perks.vals.length; i++) {
        if (lomake.perks.vals[i] == "Family heirloom (+4)" && equipment_amount < lomake.equipment.vals.length) {
            lomake.equipment.vals[equipment_amount] = "Family heirloom (4 to 8 levels)";
            equipment_amount++;
        }
        if (is_grandsahen_perk(lomake.perks.vals[i]) && equipment_amount < lomake.equipment.vals.length) {
            lomake.equipment.vals[equipment_amount] = "Amulet of a Mage (spellshield 2)"
            equipment_amount++;
        }
    }

    // Equipment based on magic skills
    if (lomake.magicskills.vals[lomake.magicskills.slots.indexOf("Spellcasting")] > 2) {
        if (wealth > 1) lomake.equipment.vals[equipment_amount] = "A gem (runestone 1)";
        else lomake.equipment.vals[equipment_amount] = "Natural rock (runestone 1)";
        equipment_amount++;
    }
    if (lomake.magicskills.vals[lomake.magicskills.slots.indexOf("Runesmithing")] > 2) {
        if (wealth > 1 && wealth <= 3) lomake.equipment.vals[equipment_amount] = "Rune-scribing tools (+1)";
        else if (wealth > 3) lomake.equipment.vals[equipment_amount] = "Exquisite rune-scribing tools (+2)";
        equipment_amount++;
    }
    if (lomake.magicskills.vals[lomake.magicskills.slots.indexOf("Ritualism")] > 2) {
        if (wealth > 1) {
            lomake.equipment.vals[equipment_amount] = "Ritual knife (2)";
            equipment_amount++;
        }
        lomake.equipment.vals[equipment_amount] = "Spiritual items";
        equipment_amount++;
    }
}

function randomize_perks(concept, culture, religion, emptify, maxperks) {
	// Pick random perks and flaws, attempt to balance character creation points
	if (emptify) {
		for (var i = 0; i < lomake.perks.vals.length; i++) {
			lomake.perks.vals[i] = "";
		}
	}

    // Randomize perk, but take it only if it makes exp points move the correct direction
	var perknames = Object.getOwnPropertyNames(perks);

    var numperks = 0;
    var has_mental_perk = false;
    var has_grandsahen_perk = false;
    var has_blackhand_perk = false;
    var has_poverty_perk = false;
    var has_rich_perk = false;

    for (var i = 0; i < lomake.perks.vals.length; i++) {
        if (lomake.perks.vals[i] != "") {
            if (perks[lomake.perks.vals[i]].type == "mental") has_mental_perk = true;
            if (is_grandsahen_perk(lomake.perks.vals[i])) has_grandsahen_perk = true;
            if (is_blackhand_perk(lomake.perks.vals[i])) has_blackhand_perk = true;
            if (is_poverty_perk(lomake.perks.vals[i])) has_poverty_perk = true;
            if (is_perk_of_wealth_four(lomake.perks.vals[i]) || is_perk_of_wealth_three(lomake.perks.vals[numperks])) has_rich_perk = true;
            numperks++;
        }
    }

	var runs = 0;
	var maxruns = 15;
	while (numperks < maxperks && runs < maxruns) {
		var perkname = perknames[rand_int(0,perknames.length-1)];
		var pointbalance = lomake.exp.give_exp - calculate_cost();

        // Allow only one mental perk
        if (has_mental_perk == true && perks[perkname].type == "mental") perkname = "";
        else if (perks[perkname].type == "mental") has_mental_perk = true;

        if (concept.unlikely_perks != null) {
            for (i = 0; i < concept.unlikely_perks.length; i++) {
                if (concept.unlikely_perks[i] == perkname) {
                    if (rand_int(1,10) >= 2) perkname = "";
                }
            }
        }
        if (culture.unlikely_perks != null) {
            for (i = 0; i < culture.unlikely_perks.length; i++) {
                if (culture.unlikely_perks[i] == perkname) {
                    if (rand_int(1,10) >= 2) perkname = "";
                }
            }
        }
        if (religion.unlikely_perks != null) {
            for (i = 0; i < religion.unlikely_perks.length; i++) {
                if (religion.unlikely_perks[i] == perkname) {
                    if (rand_int(1,10) >= 2) perkname = "";
                }
            }
        }

        if (is_grandsahen_perk(perkname) && has_blackhand_perk && rand_int(1,10) >= 5) perkname = "";
        if (is_blackhand_perk(perkname) && has_grandsahen_perk && rand_int(1,10) >= 5) perkname = "";
        if ((is_perk_of_wealth_four(perkname) || is_perk_of_wealth_three(perkname)) && has_poverty_perk) perkname = "";
        if (is_poverty_perk(perkname) && has_rich_perk) perkname = "";

		if (lomake.perks.vals.indexOf(perkname) == -1 && pointbalance < 0 && parseInt(perks[perkname].cost) <= 0){
			lomake.perks.vals[numperks] = perkname;
			numperks++;
		} else if (lomake.perks.vals.indexOf(perkname) == -1 && pointbalance > 0 && parseInt(perks[perkname].cost) >= 0){
			lomake.perks.vals[numperks] = perkname;
			numperks++;
		} else if (pointbalance == 0) return;

        if (is_grandsahen_perk(lomake.perks.vals[numperks])) has_grandsahen_perk = true;
        if (is_blackhand_perk(lomake.perks.vals[numperks])) has_blackhand_perk = true;
        if (is_poverty_perk(lomake.perks.vals[numperks])) has_poverty_perk = true;
        if (is_perk_of_wealth_four(lomake.perks.vals[numperks]) || is_perk_of_wealth_three(lomake.perks.vals[numperks])) has_rich_perk = true;

		runs++;
	}

    render_lomake();
    remove_lower_sahen_titles();
    remove_lower_wealth_titles();
    remove_lower_volar_titles();
    remove_duplicates(lomake.perks.vals);
    sort_perks();
}


function randomize_magic(creationpoints, emptify) {
	// Pick random effects and qualities based on attributes and magic skills
	// ritualism, runesmithing --> conditional quality
	if (emptify) {
		for (var i=0; i<lomake.effectsandqualities.vals.length; i++) {
			lomake.effectsandqualities.vals[i] = "";
		}
	}
	var target_amount_of_effects_and_qualities = 0;

    // Set the target amount of effects and qualities
    if (parseInt(lomake.magicskills.vals[0]) > parseInt(lomake.magicskills.vals[1])) {
        target_amount_of_effects_and_qualities += parseInt(lomake.magicskills.vals[0]);
        target_amount_of_effects_and_qualities += parseInt(lomake.magicskills.vals[1]) / 2;
    }
    else {
        target_amount_of_effects_and_qualities += parseInt(lomake.magicskills.vals[1]);
        target_amount_of_effects_and_qualities += parseInt(lomake.magicskills.vals[0]) / 2;
    }
    if (target_amount_of_effects_and_qualities > lomake.effectsandqualities.vals.length)
        target_amount_of_effects_and_qualities = lomake.effectsandqualities.vals.length;

	var attributes_sorter = [
        {id_number: 0, abbreviation: "STR", value: 1},
        {id_number: 1, abbreviation: "DEX", value: 1},
        {id_number: 2, abbreviation: "STA", value: 1},
        {id_number: 3, abbreviation: "CHA", value: 1},
        {id_number: 4, abbreviation: "INT", value: 1},
        {id_number: 5, abbreviation: "WIL", value: 1},
        {id_number: 6, abbreviation: "SEN", value: 1}
    ];

    // attribute^4, calculate sum of these values to get portion later
    var attributes_modified_overall = 0;
    for (var i=0; i<attributes_sorter.length; i++) {
        attributes_sorter[i].value = Math.pow(lomake.attributes.vals[i], 4);
        attributes_modified_overall += attributes_sorter[i].value;
    }
    // relative proportion of overall value (percentage chance)
    for (var i=0; i<attributes_sorter.length; i++) {
        attributes_sorter[i].value = attributes_sorter[i].value / attributes_modified_overall;
    }
    // under this value
    var temporary_adder = 0;
    for (var i=0; i<attributes_sorter.length; i++) {
        temporary_adder += attributes_sorter[i].value;
        attributes_sorter[i].value = temporary_adder;
    }

	// Find which effects belong to which attribute
	var effectsbyattr = []; // array of arrays, in the order that attributes appear on the sheet
	for (var i=0; i<lomake.attributes.vals.length; i++) {
		effectsbyattr.push([]);
	}

	var effectnames = Object.getOwnPropertyNames(effectsandqualities);
	for (var i=0; i<effectnames.length; i++) {
		// put effect into array under correct index
		if (lomake.attributes.slots.indexOf(effectsandqualities[effectnames[i]].attribute) != -1 && effectsandqualities[effectnames[i]].type == "effect" )
            effectsbyattr[lomake.attributes.slots.indexOf(effectsandqualities[effectnames[i]].attribute)].push(effectnames[i]);
	}

	// Choose spell effects to give the character
	var effect_points = target_amount_of_effects_and_qualities / 2;
    var use_up_to_fraction_of_points = 1.1;
    var available = creationpoints - calculate_cost();
    var slot_counter = 0;
    while (effect_points > 0) {
        var effect_integer = -1;
        var randnumm = Math.random();
        for (var i=0; i<attributes_sorter.length; i++) {
            if (randnumm < attributes_sorter[i].value) {
                effect_integer = i;
                break;
            }
        }

        // Give effect to character
        available = creationpoints - calculate_cost();
        if (use_up_to_fraction_of_points * available > 3 && effect_integer >= 0)
        {
            var int_random = rand_int(0, effectsbyattr[effect_integer].length - 1);
            lomake.effectsandqualities.vals[slot_counter] = effectsbyattr[effect_integer][int_random];
            slot_counter++;
        }
		effect_points -= 1;
    }

	// Randomize spell qualities
	var quality_points = target_amount_of_effects_and_qualities - effect_points;
	var quanames = Object.getOwnPropertyNames(effectsandqualities);
	while (quality_points > 0) {

		// Pick quality
		var this_quality = effectsandqualities[quanames[rand_int(0, quanames.length - 1)]];
		// if quality not chosen already
		// "Conditional spell" only for runesmiths and ritualists
		if (lomake.effectsandqualities.vals.indexOf(this_quality.name) && this_quality.type == "quality" &&
            !(this_quality.name == "Conditional spell" && lomake.magicskills.vals[1]==0 && lomake.magicskills.vals[2] == 0)) {
			// Pick random level of each quality
            available = creationpoints - calculate_cost();
			if (use_up_to_fraction_of_points * available > 3) lomake.effectsandqualities.vals[slot_counter] = this_quality.name;
			slot_counter++;
		}
        quality_points -= 1;
	}
    remove_duplicates(lomake.effectsandqualities.vals);
}

function randomize_favoriteskills (favor, minimum, maximum, creationpoints, use_up_to_fraction_of_points) {
    var available = 0;

    // Attempt to put a biased random values in the slot, if the name in favor is correctly spelled and available
    for (var i = 0; i < favor.length; i++) {

        available = creationpoints * use_up_to_fraction_of_points - calculate_cost();

        // TODO: Get these values from actual cost data
        if (available < 45 && maximum > 9) maximum = 9;
        if (available < 36 && maximum > 8) maximum = 8;
        if (available < 28 && maximum > 7) maximum = 7;
        if (available < 21 && maximum > 6) maximum = 6;
        if (available < 15 && maximum > 5) maximum = 5;
        if (available < 10 && maximum > 4) maximum = 4;
        if (available < 6 && maximum > 3) maximum = 3;
        if (available < 3 && maximum > 2) maximum = 2;

        if (lomake.magicskills.slots.indexOf(favor[i]) > -1) {
            var slot = lomake.magicskills.slots.indexOf(favor[i]);
            if (slot > -1) {
                lomake.magicskills.vals[slot] = rand_int(minimum,maximum);
            } else {
                console.log("typo in favored skills:", favor[i]);
            }
        } else {
            var slot = lomake.skills.slots.indexOf(favor[i]);
            if (slot > -1) {
                lomake.skills.vals[slot] = rand_int(minimum,maximum);
            } else {
                console.log("typo in favored skills:", favor[i]);
            }
        }
        if (creationpoints * use_up_to_fraction_of_points < calculate_cost()) i = 100;
    }
}


function randomize_favorite_skills(favorconcept, favorculture, favorreligion, creationpoints, emptify) {
	// Randomize all, then put minimum to maximum points to core skills
	// For Bliaron B205 character sheet

	//console.log(favor);
    var use_up_to_fraction_of_points = 0.4;

	if (emptify){
		for (var i = 0; i < lomake.skills.vals.length; i++){
            lomake.skills.vals[i] = 0;
		}
		for (var i = 0; i < lomake.magicskills.vals.length; i++){
            lomake.magicskills.vals[i] = 0;
		}
	}


    //Culture skills have lower max than religion skills
    var minimum = 1;
	var maximum = Math.floor(creationpoints / 30);
    if (minimum > 8) minimum = 8;
    if (maximum > 10) maximum = 10;
    if (favorculture != null) randomize_favoriteskills(favorculture, minimum, maximum, creationpoints, use_up_to_fraction_of_points);

    use_up_to_fraction_of_points = 0.6;
	maximum = Math.floor(creationpoints / 20);
    if (minimum > 8) minimum = 8;
    if (maximum > 10) maximum = 10;
    if (favorreligion != null) randomize_favoriteskills(favorreligion, minimum, maximum, creationpoints, use_up_to_fraction_of_points);

    use_up_to_fraction_of_points = 0.8;
    //Concept skills have higher values than culture and religion skills
    minimum = Math.floor(creationpoints / 20);
	maximum = Math.floor(creationpoints / 10);
    if (minimum > 8) minimum = 8;
    if (maximum > 10) maximum = 10;
    if (favorconcept != null) randomize_favoriteskills(favorconcept, minimum, maximum, creationpoints, use_up_to_fraction_of_points);
}

function randomize_attributes(favor){

	// For Bliaron B203 character sheet
	var used = 0;
	for (var i=0; i<lomake.attributes.vals.length; i++){
		// First remove old values
		if (favor != undefined && favor.indexOf(i) != -1){ // if is in list of attributes to favor, start at 2
			lomake.attributes.vals[i] = 2;
			lomake.attributes.creation_vals[i] = 2;
			used ++;
		} else { // Otherwise just start at 1
			lomake.attributes.vals[i] = 1;
			lomake.attributes.creation_vals[i] = 1;
		}

	}
	// console.log(lomake.attributes.vals, used );
	var pool = lomake.attributes.special_cost_pool - lomake.attributes.vals.length - used;
	for (var i=0; i<pool; i++){
		// Pick random attribute to increment
		var target = rand_int(0,lomake.attributes.vals.length-1);
		if (lomake.attributes.vals[target] < 4){
			lomake.attributes.vals[target]++;
			lomake.attributes.creation_vals[target]++;
		} else {
			target = findlowest(lomake.attributes);
			lomake.attributes.vals[target]++;
			lomake.attributes.creation_vals[target]++;
		}
	}
	render_lomake();
	backup();
}
function findhighest(field) {
	// return index in a lomake field with lowest val
	return field.vals.indexOf(Math.max.apply(null, field.vals));
}

function findlowest(field) {
	// return index in a lomake field with lowest val
	return field.vals.indexOf(Math.min.apply(null, field.vals));
}
function pick_from_array(ar) {
	// Randomly pick an entry in an array
	return ar[rand_int(0, ar.length-1)];
}
function pick_from_dictionary(dict) {
	// Randomly pick from a given dictionary type object, returns field name not contents


	var entries = Object.getOwnPropertyNames(dict);
	var ei = rand_int(0, entries.length-1);
	return entries[ei];

}
// Faction -> Ranks
var Factions = {// Currently not used
	"Grand Sahen": {

		"Oathbreaker": "Sahen member accused of misdemeanor",
		"Castrated": "Someone made incapable of magic use",
		"Brother": "A Sahen member without magical potential",
		"Noted one": "Someone who is monitored for magical potential by a Sahen",
		"Apprentice (0th grade)": "Untrained magic user",
		"Novice (1⁄2th grade)": "Beginner mage who has completed academy",

		"Guard": "Basic guardsman",
		"Bodyguard": "Personal guard for highly titled mages",
		"Bookkeeper": "Accountant, bureaucrat",
		"Courier": "Messenger, traveler",
		"Sage": "Holder of special knowledge, researcher",

		"1st grade Mage": "Fully authorized magic user",
		"Healer": "1st grade sanitarium mage specialized in healing",
		"Monk": "1st grade temple mage attending to various odd jobs",
		"Seeker": "1st grade mage who searches potential magic users",
		"Alchemist": "1st grade mage specialized in herbalism",

		"2nd grade Mage": "2nd grade Mage who is awarded special rights",
		"Flesh Sorcerer": "2nd grade Mage with permits for body alteration and complex healing",
		"Rune expert": "2nd grade Mage with complex runesmithing and castration permits",
		"Enforcer": "2nd grade Combat mage, blackhand hunter, executor",
		"Scribe": "2nd grade Mage, Holder of magical books and documents",

		"Master (3rd grade)": "Sahen Teacher, a specialist of spellcasting",
		"Archivist": "3rd grade Mage, leader librarian, master of bureaucracy",

		"Grandmaster (4th grade)": "Old and powerful, overseer of masters",
		"Archmage": "4th grade Mage with personal manor, immunity for most offenses",
		"Archmaster": "4th grade Archmage given the leadership of an academy",
		"Grandmaster": "4th grade Mage with Position in the High Council",
		"Arch-Grandmaster": "4th grade Archmage with a position in high council",

		"Immortal master (5th grade)": "The great leader"
	},
	"Blackhands": {
		"Blackhand Elder": "Rich magic user with estate in the city of Artan",
		"Blackhand": "Illegal magic user",
	}
}
// For easier character concept favored attributes
var str = 0;
var dex = 1;
var sta = 2;
var cha = 3;
var intl = 4;
var will = 5;
var sens = 6;

var cultures = [
    {name: "Lost Tribes",
    religions: ["Spiritual Determinism"],
    skills: ["Ritualism", "Survival", "Animal handling", "Medicine"],
    unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)", "Landowner (+5)", "Noble (+8)"] },
    {name: "Bliwon, rural",
    religions: ["Dogma of the Grand Sahen"],
    skills: ["Animal handling", "Survival", "Spellcasting", "Athletics"],
    unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)"] },
    {name: "Bliwon, urban",
    religions: ["Dogma of the Grand Sahen"],
    skills: ["Spellcasting", "Runesmithing","Negotiation","Seduction", "Lore"] },
    {name: "Cirilian",
    religions: ["Mehfeje Worshipper"],
    skills: ["Negotiation", "Intimidation"],
    unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)"] },
    {name: "Horucian",
    likely_has_perks: ["Tribal Sarasian (+1)"],
    religions: ["Gartanese"],
    skills: ["Negotiation", "Intimidation", "Seduction", "Lore"],
    unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)"] },
    {name: "Nomadic",
    likely_has_perks: ["Member of a Tribe (0)"],
    religions: ["Spiritual Determinism", "Animalism", "Demonism", "Shamanism"],
    skills: ["Ritualism", "Survival", "Animal handling"],
    unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)", "Landowner (+5)", "Noble (+8)"] },
    {name: "Kalith",
    likely_has_perks: ["Member of a Tribe (0)"],
    religions: ["Spiritual Determinism"],
    skills: ["Ritualism","Seafaring","Survival"],
    unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)", "Landowner (+5)", "Noble (+8)"] },
    {name: "Outcasts",
    likely_has_perks: ["Volar mage (+3)", "Volar apprentice (+1)"],
    skills: ["Spellcasting", "Runesmithing", "Survival", "Animal handling", "Seafaring"],
    unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)"] },
    {name: "Sarasian",
    religions: ["Demonism", "Animalism", "Glaryse Worshipper", "Mehfeje Worshipper", "Gartanese", "Spiritual Determinism", "Shamanism"],
    must_have_perks: ["Tribal Sarasian (+1)"],
    skills: ["Melee", "Unarmed", "Survival"],
    unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)", "Landowner (+5)"]}
];

var religions = [
    {name: "Demonism",
    must_have_perks: ["Cultist (+1)"],
    skills: ["Ritualism"],
    likely_has_perks: ["Got the Call (0)","Bliwon Blackhand (+1)","Hunted Blackhand (-3)","Marked as Blackhand (-5)",
    "Blackhand authority (+4)", "Investigated (-1)"]},
    {name: "Animalism",
    skills: ["Ritualism"],
    likely_has_perks: ["Member of a Tribe (0)"] },
    {name: "Dogma of the Grand Sahen",
    likely_has_perks: ["Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)"],
    unlikely_perks: ["Blackhand authority (+4)", "Artan Blackhand (+1)", "Bliwon Blackhand (+1)", "Hunted Blackhand (-3)",
    "Marked as Blackhand (-5)"],
    skills: ["Intimidation"] },
    {name: "Glaryse Worshipper",
    skills: ["Spellcasting", "Lore"]},
    {name: "Mehfeje Worshipper",
    skills: ["Runesmithing"]},
    {name: "Gartanese",
    skills: ["Animal handling", "Crafts"]},
    {name: "Spiritual Determinism",
    skills: ["Ritualism"]},
    {name: "Shamanism",
    skills: ["Ritualism"]}
];

var concepts = [ // This is used currently
[ // 0
	{name:"Beggar",
		attrs:[sens,sta,dex],
		skills:["Sleight of Hand", "Stealth", "Survival", "Unarmed"],
        concept_experience: 40,
        concept_flags: ["Non-awakened"],
		mental:["Amnesia","Cunning","Delusional","Pacifist"],
        must_have_perks: ["Beggar (-2)"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)", "Noble (+8)", "Well-off (+3)", "Landowner (+5)"]},
	{name:"Clan member (Steppe)",
		attrs:[dex,str,sta,will],
		skills:["Survival","Ranged", "Animal handling","Athletics","Stealth","Unarmed"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Obsession","Prick","Servant","Thrill-seeker","Sadistic","Code of Honor","Impulsive"],
        must_have_perks: ["Member of a Tribe (0)"],
        likely_cultures: ["Nomadic"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)","Volar mage (+3)", "Volar apprentice (+1)", "Noble (+8)", "Landowner (+5)"] },
	{name:"Aspiring blackhand",
		attrs:[intl,str],
		skills:["Spellcasting","Ritualism","Stealth","Unarmed","Intimidation","Lore"],
        concept_experience: 50,
        concept_flags: [""],
		mental:["Cunning","Delusional","Impulsive","Prick","Thrill-seeker"],
        must_have_perks: [""],
        likely_has_perks: ["Got the Call (0)","Bliwon Blackhand (+1)","Hunted Blackhand (-3)","Marked as Blackhand (-5)",
        "Blackhand authority (+4)", "Investigated (-1)"],
        likely_cultures: ["Bliwon, urban", "Bliwon, rural"],
        unlikely_religions: ["Dogma of the Grand Sahen"],
        unlikely_perks: "Blackhand hunter (+4)" },
	{name:"Hunter",
		attrs:[str,sta,dex,sens],
		skills:["Animal handling","Ranged","Survival","Medicine","Notice","Stealth","Survival"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Code of Honor","Direct","Thrill-seeker"],
        must_have_perks: [],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Noble (+8)"] },
	{name:"Monk",
		attrs:[intl,sta,will],
		skills:["Lore","Seduction","Medicine","Notice","Spellcasting"],
        concept_experience: 50,
        concept_flags: [""],
		mental:["Pacifist","Servant","Authoritarian"],
        must_have_perks: [""],
        likely_has_perks: ["Sahen 2nd grade (+4)", "Sahen 1st grade (+2)","Volar mage (+3)", "Volar apprentice (+1)", "Sahen novice (+1)"],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)"] },
	{name:"Mudlark",
		attrs:[dex,sta,sens],
		skills:["Sleight of Hand", "Stealth", "Survival", "Unarmed"],
        concept_experience: 40,
        concept_flags: ["Non-awakened"],
		mental:["Amnesia","Direct","Love sick"],
        must_have_perks: ["Beggar (-2)"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Noble (+8)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)", "Volar mage (+3)", "Landowner (+5)"] },
	{name:"Pickpocket",
		attrs:[dex,sens],
		skills:["Sleight of Hand", "Stealth", "Survival", "Unarmed"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Cunning","Direct","Impulsive","Seducer","Thrill-seeker"],
        must_have_perks: [""],
        likely_has_perks: ["Outlaw (-2)","Marked (-2)","Investigated (-1)"],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Noble (+8)"] },
	{name:"Slave",
		attrs:[sta,str,dex],
		skills:["Athletics","Animal handling","Unarmed","Survival","Stealth","Crafts"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Amnesia","Delusional","Authoritarian","Servant","Love sick"],
        likely_cultures: ["Cirilian", "Nomadic", "Kalith"],
        must_have_perks: ["Slave (-4)"],
        likely_has_perks: ["Marked (-2)"],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Well-off (+3)", "Noble (+8)", "Landowner (+5)"] },
	{name:"Traveling peddler",
		attrs:[cha,sta,sens],
		skills:["Animal handling","Stealth","Notice","Seduction"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Pacifist","Seducer","Cunning","Thinker"],
        must_have_perks: [""],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Noble (+8)", "Landowner (+5)"] },
	{name:"Peasant",
		attrs:[str,dex,sta],
		skills:["Crafts","Animal handling","Survival","Unarmed"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Pacifist","Servant","Idealist","Direct"],
        must_have_perks: [""],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Noble (+8)"] },
	{name:"Outlaw",
		attrs:[dex,cha,str],
		skills:["Animal handling","Ranged","Unarmed","Melee","Athletics","Notice","Seafaring","Intimidation"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Prick","Seducer","Thrill-seeker","Sadistic","Impulsive","Cunning"],
        must_have_perks: [],
        likely_has_perks: ["Outlaw (-2)","Marked (-2)","Investigated (-1)"],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Noble (+8)"] },
	{name:"Shaman apprentice",
		attrs:[will,sta,intl],
		skills:["Ritualism","Lore","Crafts","Survival","Stealth","Medicine"],
        concept_experience: 50,
        concept_flags: [""],
		mental:["Love sick","Servant","Delusional","Pacifist"],
        likely_cultures: ["Nomadic"],
        unlikely_religions: ["Dogma of the Grand Sahen"],
        must_have_perks: [""],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)","Volar mage (+3)", "Volar apprentice (+1)", "Landowner (+5)", "Noble (+8)"] }
],
[ // 1
	{name:"Nomad",
		attrs:[sta,sens,will],
		skills:["Animal handling","Ranged","Athletics","Medicine","Survival","Unarmed"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Impulsive","Code of Honor","Authoritarian","Passionate"],
        likely_cultures: ["Nomadic"],
        unlikely_cultures: ["Bliwon, urban"],
        must_have_perks: [""],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)","Volar mage (+3)", "Volar apprentice (+1)", "Well-off (+3)", "Noble (+8)", "Landowner (+5)"] },
	{name:"Burglar",
		attrs:[dex,sens,intl],
		skills:["Unarmed","Intimidation","Stealth","Sleight of Hand", "Notice","Athletics"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Cunning","Sadistic","Thrill-seeker","Direct"],
        must_have_perks: [""],
        likely_has_perks: ["Outlaw (-2)","Marked (-2)","Investigated (-1)"],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Volar mage (+3)"] },
	{name:"Bard",
		attrs:[cha,intl,sens,dex],
		skills:["Seduction","Lore","Notice"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Seducer","Impulsive","Servant"],
        must_have_perks: [""],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)"],
        equipment: ["Lute"] },
	{name:"Artist",
		attrs:[dex,sens,intl,cha],
		skills:["Seduction","Lore","Notice"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Easily hurt","Impulsive","Servant"],
        must_have_perks: [""],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)"] },
	{name:"City guard",
		attrs:[dex,str,sta],
		skills:["Melee","Unarmed","Athletics","Notice","Survival","Ranged"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Violent","Authoritarian","Prick","Servant"],
        must_have_perks: [""],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Volar mage (+3)"]  },
	{name:"Cultist",
		attrs:[will,intl,sens],
		skills:["Spellcasting", "Ritualism","Runesmithing","Stealth","Seduction","Medicine","Lore"],
        concept_experience: 50,
        concept_flags: [""],
		mental:["Cunning","Obsession","Pleasure-seeker","Thinker"],
        must_have_perks: ["Cultist (+1)"],
        likely_has_perks: ["Investigated (-1)"] },
	{name:"Farmer",
		attrs:[dex,sta,str],
		skills:["Animal handling","Crafts","Survival","Unarmed","Notice","Medicine"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Servant","Pacifist","Impulsive"],
        must_have_perks: [""],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Volar mage (+3)", "Noble (+8)"] },
	{name:"Fisherman",
		attrs:[dex,sta,str],
		skills:["Seafaring","Crafts","Survival","Unarmed","Notice","Medicine"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Servant","Pacifist","Attention seeker","Thrill-seeker"],
        must_have_perks: [""],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Volar mage (+3)", "Landowner (+5)", "Noble (+8)"] },
	{name:"Pilgrim",
		attrs:[sta,intl,will],
		skills:["Lore","Animal handling","Medicine","Survival","Unarmed","Ritualism"],
        concept_experience: 50,
        concept_flags: [""],
		mental:["Obsession","Servant","Delusional","Direct","Thinker"],
        must_have_perks: [""],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)"] },
	{name:"Poet",
		attrs:[cha,intl,sens],
		skills:["Seduction","Lore","Notice"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Thinker","Seducer","Servant"],
        must_have_perks: [""],
        likely_has_perks: [""] },
	{name:"Sailor",
		attrs:[dex,str,sta],
		skills:["Seafaring", "Athletics","Lore","Notice","Unarmed","Survival"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Thrill-seeker","Amnesia","Prick","Impulsive"],
        must_have_perks: [""],
        unlikely_cultures: ["Cirilian"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)"] },
	{name:"Servant",
		attrs:[dex,intl,sens],
		skills:["Animal handling","Crafts","Medicine","Stealth","Unarmed","Melee"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Servant","Pacifist","Seducer"],
        must_have_perks: [""],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Noble (+8)", "Well-off (+3)", "Landowner (+5)"] },
	{name:"Smith",
		attrs:[dex,str,sta,intl],
		skills:["Crafts","Medicine","Survival","Unarmed","Seduction","Melee"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Code of Honor","Thinker","Servant","Passionate"],
        must_have_perks: [""],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Noble (+8)"] },
	{name:"Soldier",
		attrs:[dex,str,sta],
		skills:["Melee","Unarmed","Ranged","Survival","Intimidation","Animal handling"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Prick","Code of Honor","Authoritarian","Thrill-seeker"],
        must_have_perks: [""],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)"] },
	{name:"Tribe member",
		attrs:[dex,sta,sens],
		skills:["Survival","Ranged","Crafts","Medicine","Ritualism","Stealth"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Thrill-seeker","Direct","Impulsive"],
        must_have_perks: [""],
        likely_cultures: ["Nomadic", "Lost Tribes", "Sarasian", "Horucian"],
        unlikely_cultures: ["Bliwon, urban"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)","Volar mage (+3)", "Volar apprentice (+1)", "Landowner (+5)", "Noble (+8)" ] },
	{name:"Sahen mage",
		attrs:[intl,will,cha],
		skills:["Lore","Seduction","Notice","Spellcasting"],
        concept_experience: 65,
        concept_flags: [""],
		mental:["Code of Honor","Servant","Thinker","Prick"],
        must_have_perks: ["Sahen novice (+1)"],
        likely_cultures: ["Bliwon, urban", "Bliwon, rural"],
        likely_has_perks: ["Sahen 2nd grade (+4)", "Sahen 1st grade (+2)"],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)"] },
	{name:"Housewife",
		attrs:[cha,dex,will],
		skills:["Animal handling","Crafts","Lore","Medicine","Notice","Seduction"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Seducer","Servant","Pleasure-seeker","Love sick"],
        must_have_perks: [""],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)","Volar mage (+3)", "Volar apprentice (+1)"] },
	{name:"Demon cultist",
		attrs:[will,intl],
		skills:["Spellcasting","Ritualism","Sleight of Hand","Unarmed","Lore"],
        concept_experience: 50,
        concept_flags: [""],
		mental:["Violent","Prick","Seducer","Cunning","Obsession","Delusional"],
        must_have_perks: ["Cultist (+1)"],
        unlikely_religions: ["Dogma of the Grand Sahen"],
        likely_has_perks: ["Investigated (-1)"] },
	{name:"Sahen Novice",
		attrs:[intl,will],
		skills:["Spellcasting","Unarmed","Lore","Seduction"],
        concept_experience: 40,
        concept_flags: [""],
		mental:["Prick","Authoritarian","Servant","Code of Honor"],
        must_have_perks: ["Sahen novice (+1)"],
        likely_cultures: ["Bliwon, urban", "Bliwon, rural"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)","Volar mage (+3)"] },
	{name:"Deliveryman",
		attrs:[dex,str,sta],
		skills:["Animal handling","Notice","Athletics","Stealth","Melee","Unarmed"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Servant","Thrill-seeker","Love sick","Impulsive"],
        must_have_perks: [""],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)","Noble (+8)", "Well-off (+3)", "Landowner (+5)"] }
],
[	// 2
	{name:"Craftman",
		attrs:[dex,intl,str],
		skills:["Crafts","Seduction","Notice","Unarmed"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Code of Honor","Obsession","Thinker","Passionate"],
        must_have_perks: ["Well-off (+3)"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)", "Noble (+8)"] },
	{name:"Tax collector",
		attrs:[cha,intl,will],
		skills:["Intimidation","Seduction","Melee","Unarmed","Animal handling"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Prick","Sadistic","Cunning","Authoritarian"],
        must_have_perks: ["Well-off (+3)"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)"] },
	{name:"Sarasian warrior",
		attrs:[dex,str,sta],
		skills:["Animal handling","Ranged","Athletics","Intimidation","Melee","Survival","Unarmed"],
        concept_experience: 60,
        concept_flags: ["Non-awakened"],
		mental:["Violent","Prick","Thrill-seeker","Code of Honor"],
        must_have_perks: ["Tribal Sarasian (+1)"],
        likely_cultures: ["Sarasian"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)"] },
	{name:"Shaman",
		attrs:[intl,will,cha],
		skills:["Ritualism","Medicine","Notice","Survival"],
        concept_experience: 65,
        concept_flags: [""],
		mental:["Amnesia","Delusional","Cunning","Authoritarian"],
        must_have_perks: [""],
        likely_cultures: ["Nomadic"],
        unlikely_religions: ["Dogma of the Grand Sahen"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)","Volar mage (+3)", "Volar apprentice (+1)", "Landowner (+5)", "Noble (+8)"] },
	{name:"Shop owner",
		attrs:[intl,sens,cha],
		skills:["Seduction","Lore","Animal handling","Medicine","Unarmed"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Authoritarian","Pacifist","Pleasure-seeker","Direct"],
        must_have_perks: ["Well-off (+3)"],
        likely_has_perks: ["Landowner (+5)"],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)"] },
    {name:"Tribal chieftain",
    	attrs:[str,dex,will],
    	skills:["Intimidation", "Melee", "Unarmed", "Animal handling"],
        concept_experience: 65,
        concept_flags: ["Non-awakened"],
    	mental:["Authoritarian", "Direct", "Code of Honor"],
        must_have_perks: ["Chief (+7)"],
        likely_cultures: ["Nomadic", "Sarasian", "Lost Tribes"],
        likely_has_perks: ["Hero (+2)", "Scarred (0)", "Huge (+5)"],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)","Volar mage (+3)", "Volar apprentice (+1)"] },
	{name:"Spy",
		attrs:[cha,intl,dex],
		skills:["Melee","Seduction","Stealth","Intimidation","Unarmed"],
        concept_experience: 65,
        concept_flags: [""],
		mental:["Cunning","Direct","Servant","Seducer"],
        must_have_perks: ["Secret mission (-2)"],
        likely_has_perks: ["Well-off (+3)"],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)"] },
	{name:"Volar adept",
		attrs:[intl,will],
		skills:["Spellcasting","Runesmithing","Lore","Medicine","Survival","Notice"],
        concept_experience: 50,
        concept_flags: [""],
		mental:["Authoritarian","Code of Honor","Thinker","Obsession"],
        must_have_perks: ["Volar apprentice (+1)"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)"] },
	{name:"Trusted bodyguard",
		attrs:[dex,str,intl],
		skills:["Melee","Unarmed","Ranged","Athletics","Stealth"],
        concept_experience: 65,
        concept_flags: ["Non-awakened"],
		mental:["Servant","Code of Honor","Love sick","Prick"],
        must_have_perks: ["Well-off (+3)"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Noble (+8)"] },
	{name:"Blackhand (Bliwon)",
		attrs:[intl,cha],
		skills:["Seduction","Spellcasting","Ritualism","Melee","Notice","Lore"],
        concept_experience: 65,
        concept_flags: [""],
		mental:["Cunning","Delusional","Sadistic","Authoritarian"],
        likely_cultures: ["Bliwon, urban", "Bliwon, rural"],
        unlikely_cultures: ["Horucian"],
        unlikely_religions: ["Dogma of the Grand Sahen"],
        must_have_perks: ["Bliwon Blackhand (+1)"],
        likely_has_perks: ["Well-off (+3)"],
        unlikely_perks: ["Artan Blackhand (+1)"] },
	{name:"Family Blackhand (Artan)",
		attrs:[cha,intl,will],
		skills:["Seduction","Intimidation","Spellcasting","Ritualism","Lore"],
        concept_experience: 65,
        concept_flags: [""],
		mental:["Cunning","Prick","Cunning","Pleasure-seeker"],
        likely_cultures: ["Horucian"],
        unlikely_cultures: ["Bliwon, urban", "Bliwon, rural"],
        unlikely_religions: ["Dogma of the Grand Sahen"],
        must_have_perks: ["Artan Blackhand (+1)"],
        likely_has_perks: ["Well-off (+3)"],
        unlikely_perks: ["Bliwon Blackhand (+1)"] },
	{name:"Sahen Seeker",
		attrs:[sens,will,intl],
		skills:["Spellcasting","Notice","Unarmed","Survival","Lore","Seduction"],
        concept_experience: 65,
        concept_flags: [""],
		mental:["Obsession","Servant","Authoritarian","Seducer"],
        likely_cultures: ["Bliwon, urban", "Bliwon, rural"],
        must_have_perks: ["Sahen 1st grade (+2)"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Noble (+8)"] },
	{name:"Sahen Enforcer",
		attrs:[intl,str,dex],
		skills:["Spellcasting","Notice","Unarmed","Melee","Intimidation","Stealth"],
        concept_experience: 65,
        concept_flags: [""],
		mental:["Prick","Code of Honor","Cunning","Thrill-seeker"],
        likely_cultures: ["Bliwon, urban", "Bliwon, rural"],
        must_have_perks: ["Sahen 2nd grade (+4)"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Noble (+8)"],
        equipment: ["Belt of Wind Stones (10 x runestone lvl 1)", "The Spear of the Enforcer (2)"] },
	{name:"Danak tamer",
		attrs:[intl,str,dex],
		skills:["Animal handling","Notice","Spellcasting","Unarmed","Melee","Intimidation","Survival"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Thrill-seeker","Sadistic","Authoritarian","Code of Honor","Impulsive"],
        must_have_perks: [""],
        likely_has_perks: ["Member of a Tribe (0)"],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)","Volar mage (+3)", "Volar apprentice (+1)"] },
	{name:"Sahen Courier",
		attrs:[dex,sta,intl],
		skills:["Animal handling","Athletics","Lore","Stealth","Survival","Spellcasting"],
        concept_experience: 50,
        concept_flags: [""],
		mental:["Servant","Pleasure-seeker","Pacifist","Direct"],
        likely_cultures: ["Bliwon, urban", "Bliwon, rural"],
        must_have_perks: ["Sahen 1st grade (+2)"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Noble (+8)"] },
	{name:"Sahen Sage",
		attrs:[intl,sens,will],
		skills:["Lore","Medicine","Notice","Seduction","Stealth","Spellcasting"],
        concept_experience: 65,
        concept_flags: [""],
		mental:["Thinker","Servant","Authoritarian"],
        must_have_perks: ["Sahen 2nd grade (+4)"],
        likely_cultures: ["Bliwon, urban", "Bliwon, rural"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)"] }

],
[	// 3
	{name:"Landowner",
		attrs:[intl,cha],
		skills:["Seduction","Intimidation","Lore","Animal handling","Unarmed"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Prick","Pleasure-seeker","Seducer"],
        must_have_perks: ["Landowner (+5)"],
        likely_cultures: ["Bliwon, urban", "Bliwon, rural", "Horucian"],
        likely_has_perks: [""] },
	{name:"Historian",
		attrs:[intl,sens,will],
		skills:["Lore","Stealth","Survival","Seafaring","Animal handling"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Thinker","Obsession","Servant","Passionate"],
        must_have_perks: [""],
        likely_cultures: ["Bliwon, urban", "Horucian"],
        likely_has_perks: ["Well-off (+3)"] },
	{name:"Kalthan researcher",
		attrs:[intl,sens,will],
		skills:["Lore","Stealth","Survival","Seafaring","Animal handling"],
        concept_experience: 65,
        concept_flags: [""],
		mental:["Thinker","Delusional","Direct","Authoritarian"],
        must_have_perks: [""],
        likely_cultures: ["Bliwon, urban", "Horucian", "Outcasts"],
        likely_has_perks: ["Well-off (+3)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)"] },
	{name:"Inheritor",
		attrs:[cha,intl],
		skills:["Seduction","Lore","Ranged","Melee","Notice","Intimidation"],
        concept_experience: 50,
        concept_flags: ["Non-awakened"],
		mental:["Seducer","Thrill-seeker","Pleasure-seeker","Love sick"],
        must_have_perks: ["Well-off (+3)"],
        likely_cultures: ["Bliwon, urban", "Bliwon, rural", "Horucian"],
        likely_has_perks: ["Landowner (+5)"] },
	{name:"Sahen Master",
		attrs:[intl,sens,will],
		skills:["Spellcasting","Lore","Seduction","Intimidation","Medicine","Runesmithing"],
        concept_experience: 80,
        concept_flags: [""],
		mental:["Authoritarian","Passionate","Code of Honor"],
        must_have_perks: ["Sahen 3rd grade (+6)"],
        likely_cultures: ["Bliwon, urban"],
        likely_has_perks: [""],
        equipment: ["Runestone of Strength (runestone 2, 2 x day)"] },
	{name:"Karma combat master",
		attrs:[dex,str,intl],
		skills:["Unarmed","Melee","Ranged","Intimidation","Stealth","Survival"],
        concept_experience: 80,
        concept_flags: [""],
		mental:["Prick","Sadistic","Code of Honor","Thrill-seeker"],
        must_have_perks: ["Blackhand hunter (+4)"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)"] }
],
[ // 4
	{name:"Blackhand elder",
		attrs:[cha,intl,will],
		skills:["Spellcasting","Runesmithing","Ritualism","Intimidation","Seduction","Lore"],
        concept_experience: 100,
        concept_flags: [""],
		mental:["Cunning","Delusional","Impulsive","Seducer"],
        likely_cultures: ["Horucian"],
        must_have_perks: ["Noble (+8)"],
        unlikely_religions: ["Dogma of the Grand Sahen"],
        likely_has_perks: [""],
        unlikely_perks: ["Sahen 4th grade (+9)", "Sahen 3rd grade (+6)", "Sahen 2nd grade (+4)", "Sahen 1st grade (+2)"] },
	{name:"Sahen Archmage",
		attrs:[intl,will,sens],
		skills:["Spellcasting","Runesmithing","Intimidation","Seduction","Lore"],
        concept_experience: 100,
        concept_flags: [""],
		mental:["Authoritarian","Cunning","Code of Honor","Thinker"],
        must_have_perks: ["Sahen 4th grade (+9)"],
        likely_cultures: ["Bliwon, urban"],
        likely_has_perks: [""],
        equipment: ["Archmage's pendant (runestone 4, 2 x day)"]},
	{name:"Noble",
		attrs:[intl,will,sens],
		skills:["Seduction","Intimidation","Lore","Melee"],
        concept_experience: 65,
        concept_flags: ["Non-awakened"],
		mental:["Thinker","Seducer","Prick","Pleasure-seeker"],
        must_have_perks: ["Noble (+8)"],
        likely_cultures: ["Bliwon, urban"],
        likely_has_perks: [""] },
    {name:"Warlord",
    	attrs:[cha,str,dex],
    	skills:["Melee","Unarmed","Intimidation","Animal handling", "Ranged"],
        concept_experience: 80,
        concept_flags: ["Non-awakened"],
    	mental:["Thinker","Seducer","Prick","Pleasure-seeker"],
        must_have_perks: ["Chief (+7)"],
        likely_cultures: ["Bliwon, urban", "Sarasian", "Cirilian"],
        likely_has_perks: ["Noble (+8)", "Huge (+5)", "Iron-boned (+4)", "Quick healer (+3)", "Scarred (0)", "Well-off (+3)"] },
	{name:"Merchant",
		attrs:[intl,will,cha],
		skills:["Seduction","Intimidation","Unarmed","Lore","Crafts"],
        concept_experience: 65,
        concept_flags: ["Non-awakened"],
		mental:["Cunning","Pleasure-seeker","Code of Honor","Authoritarian"],
        must_have_perks: [""],
        likely_cultures: ["Bliwon, urban", "Horucian", "Cirilian"],
        likely_has_perks: ["Well-off (+3)", "Noble (+8)", "Landowner (+5)"] }

]
]; // concepts

var advanced_options = [
    {name: "NPC-mode", type: "checkbox", value: 0},
    {name: "Randomize Attributes", type: "checkbox", value: 1},
    {name: "Concept-based skill randomization", type: "checkbox", value: 1},
    {name: "Balance exp use with random skills", type: "checkbox", value: 1},
    {name: "Randomize Mastered Effects and Qualities", type: "checkbox", value: 1},
    {name: "Randomize Perks", type: "checkbox", value: 1},
    {name: "Max random perks", type: "number", value: 5, max: 5}
];

var occupations = [ //jobs for parents of random characters mostly
[ // 0
	{name:"Beggar",
		attrs:[sens,sta,dex],
		skills:["Sleight of Hand", "Stealth", "Survival", "Unarmed"],
		mental:["Amnesia","Cunning","Delusional","Pacifist"]},
	{name:"Clan member (Steppe)",
		attrs:[dex,str,sta,will],
		skills:["Survival","Ranged", "Animal handling","Athletics","Stealth","Unarmed"],
		mental:["Obsession","Prick","Servant","Thrill-seeker","Sadistic","Code of Honor","Impulsive"]},
	{name:"Hunter",
		attrs:[str,sta,dex,sens],
		skills:["Animal handling","Ranged","Survival","Medicine","Notice","Stealth","Survival"],
		mental:["Code of Honor","Direct","Thrill-seeker"]},
	{name:"Monk",
		attrs:[intl,sta,will],
		skills:["Lore","Seduction","Medicine","Notice"],
		mental:["Pacifist","Servant","Code of Honor"]},
	{name:"Slave",
		attrs:[sta,str,dex],
		skills:["Athletics","Animal handling","Unarmed","Survival","Stealth","Crafts"],
		mental:["Amnesia","Delusional","Authoritarian","Servant","Love sick"]},
	{name:"Traveling peddler",
		attrs:[cha,sta,sens],
		skills:["Animal handling","Stealth","Notice","Stealth","Seduction"],
		mental:["Pacifist","Seducer","Code of Honor","Thinker"]},
	{name:"Peasant",
		attrs:[str,dex,sta],
		skills:["Crafts","Animal handling","Survival","Unarmed"],
		mental:["Pacifist","Servant","Code of Honor","Direct"]},
],
[ // 1
	{name:"Nomad",
		attrs:[sta,sens,will],
		skills:["Animal handling","Ranged","Athletics","Medicine","Survival","Unarmed"],
		mental:["Impulsive","Code of Honor","Authoritarian","Passionate"]},
	{name:"Bard",
		attrs:[cha,intl,sens,dex],
		skills:["Seduction","Lore","Notice"],
		mental:["Seducer","Impulsive","Servant"]},
	{name:"Artist",
		attrs:[dex,sens,intl,cha],
		skills:["Seduction","Lore","Notice"],
		mental:["Easily hurt","Impulsive","Servant"]},
	{name:"City guard",
		attrs:[dex,str,sta],
		skills:["Melee","Unarmed","Athletics","Notice","Survival","Ranged"],
		mental:["Violent","Code of Honor","Prick","Servant"]},
	{name:"Farmer",
		attrs:[dex,sta,str],
		skills:["Animal handling","Crafts","Survival","Unarmed","Notice","Medicine"],
		mental:["Servant","Pacifist","Code of Honor"]},
	{name:"Fisherman",
		attrs:[dex,sta,str],
		skills:["Seafaring","Crafts","Survival","Unarmed","Notice","Medicine"],
		mental:["Servant","Pacifist","Code of Honor","Thrill-seeker"]},
	{name:"Poet",
		attrs:[cha,intl,sens],
		skills:["Seduction","Lore","Notice"],
		mental:["Thinker","Seducer","Servant"]},
	{name:"Sailor",
		attrs:[dex,str,sta],
		skills:["Seafaring", "Athletics","Lore","Notice","Unarmed","Survival"],
		mental:["Thrill-seeker","Amnesia","Prick","Impulsive"]},
	{name:"Servant",
		attrs:[dex,intl,sens],
		skills:["Animal handling","Crafts","Medicine","Stealth","Unarmed","Melee"],
		mental:["Servant","Pacifist","Seducer"]},
	{name:"Smith",
		attrs:[dex,str,sta,intl],
		skills:["Crafts","Medicine","Survival","Unarmed","Seduction","Melee"],
		mental:["Code of Honor","Thinker","Servant","Passionate"]},
	{name:"Soldier",
		attrs:[dex,str,sta],
		skills:["Melee","Unarmed","Ranged","Survival","Intimidation","Animal handling"],
		mental:["Prick","Code of Honor","Authoritarian","Thrill-seeker"]},
	{name:"Tribe member",
		attrs:[dex,sta,sens],
		skills:["Survival","Ranged","Crafts","Medicine","Ritualism","Stealth"],
		mental:["Thrill-seeker","Direct","Impulsive"]},

],
[	// 2
	{name:"Craftman",
		attrs:[dex,intl,str],
		skills:["Crafts","Seduction","Notice","Unarmed"],
		mental:["Code of Honor","Obsession","Thinker","Passionate"]},
	{name:"Sarasian warrior",
		attrs:[dex,str,sta],
		skills:["Animal handling","Ranged","Athletics","Intimidation","Melee","Survival","Unarmed"],
		mental:["Violent","Prick","Thrill-seeker","Code of Honor"]},
	{name:"Shaman",
		attrs:[intl,will,cha],
		skills:["Spellcasting","Ritualism","Medicine","Notice","Survival"],
		mental:["Amnesia","Delusional","Cunning","Authoritarian"]},
	{name:"Shop owner",
		attrs:[intl,sens,cha],
		skills:["Seduction","Lore","Animal handling","Medicine","Unarmed"],
		mental:["Authoritarian","Pacifist","Pleasure-seeker","Direct"]},

],
[	// 3
	{name:"Landowner",
		attrs:[intl,cha],
		skills:["Seduction","Intimidation","Lore","Animal handling","Unarmed"],
		mental:["Prick","Pleasure-seeker","Seducer"]},

],
[ // 4

	{name:"Noble",
		attrs:[intl,will,sens],
		skills:["Seduction","Intimidation","Lore","Melee"],
		mental:["Thinker","Seducer","Prick","Pleasure-seeker"]},
	{name:"Merchant",
		attrs:[intl,will,sens],
		skills:["Seduction","Intimidation","Unarmed","Lore","Crafts"],
		mental:["Cunning","Pleasure-seeker","Code of Honor","Authoritarian"]}

]
]; // charcter parent occupations


var archetypes = { // Not used
	"Novice characters (recommended)":[
		"Rural adept (non-affiliated spellcaster)",
		"Cultist (blackhand-affiliated aspiring spellcaster)",
		"Shaman apprentice (aspiring ritualist)",
		"Sahen Novice (aspiring mage)",
		"Bodyguard (non-mage, combat character)",
	],
	"Skilled characters (advanced play, and npc examples)": [
		"Free Blackhand (Bliwon blackhand)",
		"Family Blackhand (Artan blackhand)",
		"Tribal Shaman (ritualist)",
		"Sahen Flesh Sorcerer (mage,",
		"sanitarium healer)",
		"Sahen Seeker (mage, seeks apprentices)",
		"Sahen Temple Monk (mage, offers public services)",
		"Sahen Enforcer (battlemage)",
		"Sahen Courier (envoy, deliveryman)",
		"Sahen Sage (less spellcasting, but specialized knowledge)",
		"Necromancer (dark fantasy)",
		"Demon cultist (dark fantasy)",
		"Karma member (non-mage, combat",
		"character)",
	],
	"Mighty characters (advanced play, and npc examples)": [
		"Blackhand elder (Artan elder or independent)",
		"Sacred Shaman (ritualist)",
		"Sahen Master (teacher role)",
		"Sahen Archmage (semi-independent)",
		"Karma master (non-mage, combat character)",
	],
	"Supporting characters (weaker than recommended PC’s)":[
		"Peasant (farmer, hunter, craftsman)",
		"Townsman (craftsman, trader,",
		"Investigator)",
		"Outlaw"
	]
}; // archetypes

var melee_weapons = {
	"Improvised weapon": {damage: 1, wealthrequirement: 0},
	"Wooden Staff": {damage: 2, wealthrequirement: 1},
	"Wooden spear": {damage: 3, wealthrequirement: 1},
	"Knife": {damage: 2, wealthrequirement: 1},
	"Worn out sword": {damage: 3, wealthrequirement: 1},
	"Clansmen spear": {damage: 4, wealthrequirement: 1},
	"Woodsman axe": {damage: 4, wealthrequirement: 1},
	"Bronze sword": {damage: 6, wealthrequirement: 2},
	"Ornate spear": {damage: 6, wealthrequirement: 3},
	"Steel sword": {damage: 7, wealthrequirement: 4},
	"Steel halberd": {damage: 8, wealthrequirement: 4}

}; // weapons

var archery_weapons = {
	"Javelin": {damage: 2, wealthrequirement: 0},
	"Longbow": {damage: 3, wealthrequirement: 1},
	"Recurve bow": {damage: 2, wealthrequirement: 2},
	"Crossbow": {damage: 4, wealthrequirement: 3}


}; // weapons

var armor = {
	"Leather armor, partial": {armor: 1, wealthrequirement: 1},
	"Bronze armor, partial": {armor: 2, wealthrequirement: 2},
	"Iron armor, partial": {armor: 3, wealthrequirement: 3},
	"Steel armor, partial": {armor: 4, wealthrequirement: 4},

	"Leather or fur armor": {armor: 2, wealthrequirement: 1},
	"Bronze armor": {armor: 3, wealthrequirement: 2},
	"Iron armor": {armor: 4, wealthrequirement: 3},
	"Steel armor": {armor: 5, wealthrequirement: 4}

};
var shields = {
	"Buckler shield": {armor: 2, wealthrequirement: 2},
	"Tower shield": {armor: 3, wealthrequirement: 3}
};

// TODO: Make list of occupations for parents according to wealth
// var parents	= {

// }

// Places
var places = {
	"Serito":{type:"inland",culture:"monastics",location:"Black Mountains",wealth:2,population:2000},
	"Rimmor":{type:"inland",culture:"tribal",location:"Black Mountains",wealth:1,population:0},
	"Ciril":{type:"inland",culture:"Cirilian",location:"Black Mountains",wealth:5,population:8000},
	"Renok":{type:"coastal",culture:"tribal",location:"Black Mountains",wealth:1,population:200},

	"Sopor":{type:"island",culture:"tribal",location:"Western Sea",wealth:1,population:100},
	"Tari":{type:"island",culture:"tribal",location:"Western Sea",wealth:1,population:100},
	"Nago":{type:"island",culture:"tribal",location:"Western Sea",wealth:1,population:100},

	"Quadi":{type:"island",culture:"Bliwonese",location:"Southern Sea",wealth:3,population:4000},
	"Nasir":{type:"island",culture:"Bliwonese",location:"Southern Sea",wealth:2,population:500},
	"Rinn":{type:"island",culture:"Bliwonese",location:"Southern Sea",wealth:2,population:1000},
    "Moardo":{type:"island",culture:"Bliwonese",location:"Southern Sea",wealth:2,population:100},
	"Gilatt":{type:"island",culture:"independent",location:"Southern Sea",wealth:2,population:2000},
	"east of Tarros":{type:"island",culture:"tribal",location:"Southern Sea",wealth:1,population:100},

	"Minrath":{type:"island",location:"Eastern Sea",wealth:3,population:400},

	"Gumo":{type:"inland",culture:"Bliwonese",location:"valley of Bliwon",wealth:3,population:2000},
	"coast of lake Linden":{type:"inland",culture:"Bliwonese",location:"valley of Bliwon",wealth:3,population:3000},
	"Gwathio":{type:"coastal",culture:"Bliwonese",location:"valley of Bliwon",wealth:5,population:15000},
	"Ningar":{type:"inland",culture:"Bliwonese",location:"valley of Bliwon",wealth:5,population:80000},
	"Lunin":{type:"inland",culture:"Bliwonese",location:"valley of Bliwon",wealth:5,population:10000},
	"Ondrus":{type:"coastal",culture:"Bliwonese",location:"valley of Bliwon",wealth:2,population:20},
    "Loprak":{type:"inland",culture:"Bliwonese",location:"valley of Bliwon",wealth:2,population:1500},
    "Kora Kar":{type:"inland",culture:"Bliwonese",location:"valley of Bliwon",wealth:2,population:1000},
    "Ninneve":{type:"inland",culture:"Bliwonese",location:"valley of Bliwon",wealth:2,population:1000},
    "Anshan":{type:"inland",culture:"Bliwonese",location:"valley of Bliwon",wealth:2,population:1000},
    "Susak":{type:"inland",culture:"Bliwonese",location:"valley of Bliwon",wealth:2,population:1000},
	"deep wilds of Kutang":{type:"coastal",culture:"tribal",location:"valley of Bliwon",wealth:2,population:500},

	"Artan":{type:"coastal",culture:"Horucian",location:"East Horuc",wealth:5,population:35000},
	"plains of Saras":{type:"inland",culture:"tribal",location:"East Horuc",wealth:2,population:10000},
    "spring of Tosan":{type:"inland",culture:"tribal",location:"East Horuc",wealth:2,population:100},

	"Klokk Uud":{type:"coastal",culture:"monastics",location:"East Horuc",wealth:1,population:15},

	"Hospril":{type:"inland",culture:"Horucian",location:"Ashen Coast",wealth:3,population:5000},
	"Tenrak":{type:"coastal",culture:"Horucian",location:"Ashen Coast",wealth:3,population:15000},
    "rural Ashen Coast":{type:"coastal",culture:"tribal",location:"Ashen Coast",wealth:3,population:5000},

	"deep tropics":{type:"coastal",culture:"tribal",location:"rainforest of Molem",wealth:2,population:1000},

	"Longot":{type:"coastal",culture:"tribal",location:"archipelago of Zeles",wealth:2,population:2000},
	"Korab":{type:"coastal",culture:"tribal",location:"archipelago of Zeles",wealth:2,population:3000},

    "Western Great Steppe":{type:"inland",culture:"tribal",location:"Great Steppe",wealth:2,population:1000},
    "Eastern Great Steppe":{type:"inland",culture:"tribal",location:"Great Steppe",wealth:2,population:1000},
	"Barub":{type:"inland",culture:"monastics",location:"Great Steppe",wealth:2,population:100},

	"Tollhan":{type:"coastal",culture:"independent",location:"forest of Jauron",wealth:1,population:2000},
    "Masel":{type:"coastal",culture:"independent",location:"forest of Jauron",wealth:1,population:2000},

	"Carehos":{type:"inland",culture:"Horucian",location:"plains of Horuc",wealth:5,population:50000},
	"Habis Criil":{type:"coastal",culture:"monastics",location:"plains of Horuc",wealth:2,population:10},

    "Gatos":{type:"coastal",culture:"Gartanese",location:"fjord of Gartanon",wealth:4,population:4000},
	"Bomrin":{type:"coastal",culture:"Gartanese",location:"fjord of Gartanon",wealth:4,population:6000},
};

var placenames = Object.getOwnPropertyNames(places); // Just names of places

var randomevents = [ // Things that can happen to characters
	{name: "betrayal", text:"was betrayed by"},
	{name: "death of father",text:"" },
	{name: "received object",text:"received their father's trusty" },
	{name: "apprenticeship",text:"apprenticed with",result:"learned skill" },

];

var childhood_activities = [ // by wealth class. Complete the sentence: "spent most of their early childhood " and can have an effect on skills or perks or both. TODO: Maybe also specific activities per occupation?
[
	{text:"licking pebbles", effects:{perk:"Sickly"}},
	{text:"foraging for food", effects:{skill:"Survival"}},
],[
	{text:"working with their parents", effects:{skill:"Crafts"}},
	{text:"learning to handle animals", effects:{skill:"Animal handling"}}
],[
	{text:"running around freely", effects:{skill:"Athletics"}},
	{text:"playing with the neighborhood kids",effects:{skill:"Athletics"}}
],[
	{text:"studying",effects:{skill:"Lore"}},
	{text:"representing the family at formal events",effects:{skill:"Seduction"}}
],[
	{text:"playing in the safety of the family estate", effects:{perk:"Lucky"}},
	{text:"studying court etiquette and science under a private teacher", effects:{skill:"Lore"}}
]]; // Childhood activities
