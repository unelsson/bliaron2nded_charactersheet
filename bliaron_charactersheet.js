/*bliaron_hahmolomake*/
// LGPL-3.0-or-later

// TODO: kulttuuri & uskonto dropparit
// TODO: hienommat ja isommat < > nappulat numeroille
// TODO: Kuluva attribuutti esim Voima 2/3
// TODO: Vastustuskyvyt... on jo?
// TODO: Kansa: dropdown?
// TODO: Kestopisteet + muuta tulostuskelpoiseksi
// TODO: vapaan tekstin alue joka myös tallentuisi
// TODO: Varusteet ja omaisuus
// TODO: page title hahmon nimeksi
// TODO: tulostuskelpoisuus: Älä avaa tulostusikkunaa
// TODO: piirteeseen jos kirjottaa -3 alkuisen tms niin se tyhjää koko kentän
// Miten n&auml;&auml; saadaan vaikuttamaan atribuutteihin? Kielletyt kombinaatiot?

// Find attributes by name, return number
function attr(txt) {
	return lomake.attributes.slots[lomake.attributes.slots.indexOf(txt)];
}
function skill(txt) {
	return lomake.skills.slots[lomake.skills.slots.indexOf(txt)];
}
function magic(txt) {
	return lomake.magicskills.slots[lomake.magicskills.slots.indexOf(txt)];
}
function has_perk(txt) {
	if (lomake.perks.vals.indexOf(txt) > -1){
		return 1;
	}
	return 0;
}
// Replacement table for storing numbers greater than 10 but less than 61
var alph = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
// Piirr&auml; lomake kokonaan uusiksi

function render_lomake() {

	calculate_values();
	calculate_special_cost();
	// for (var i=0; i<lomake.length; i++){
	for (var propt in lomake){
		// console.log(propt);
		var d = document.getElementById(propt);
		if (!d){
			continue; // Don't do anything if a div with this name doesn't exist.
		}
		while (d.hasChildNodes()){
			d.removeChild(d.childNodes[0]);
		}
		// If there are calculated values, use those
		if (lomake[propt].calculated && !lomake[propt].can_buy){
			var vals = lomake[propt].calculated;
		} else {
			var vals = lomake[propt].vals;
		}



		if (lomake[propt].type == "editable"){
			for (var i=0; i<lomake[propt].slots.length; i++){
				var span = d.appendChild(document.createElement("span"));
				var s = span.appendChild(document.createElement("span"));
				span.classList.add("editable-span");
				s.innerHTML = lomake[propt].slots[i];
				var n = span.appendChild(document.createElement("input"));
				n.setAttribute("onchange",'changehandler("'+target("vals",propt,i)+'",this,event);');
				n.setAttribute("onkeyup",'textchangehandler("'+target("vals",propt,i)+'",this,event);');
				if (vals[i]){
					n.value = vals[i];
				}
			}
		} else {
		// Table
		var t = d.appendChild(document.createElement("table"));
		var tdhead="";
		// title - thead
		if (lomake[propt].title){
			var caption = t.appendChild(document.createElement("caption"));
			// tdhead = thead.appendChild(document.createElement("td"));
			caption.innerHTML = lomake[propt].title;
			if (lomake[propt].special_cost_pool){
				// If there's a special char creation point system for this box, show available points
				if (lomake[propt].special_cost_used != lomake[propt].special_cost_pool){
					// If there's a special char creation point system for this box, show available points
					// td = thead.appendChild(document.createElement("td"));
					// console.log(lomake[propt].special_cost_used);
					// td.innerHTML = (parseInt(lomake[propt].special_cost_pool)-parseInt(lomake[propt].special_cost_used)) + "/" + lomake[propt].special_cost_pool;
					caption.innerHTML += " "+(parseInt(lomake[propt].special_cost_pool)-parseInt(lomake[propt].special_cost_used)) + "/" + lomake[propt].special_cost_pool;
				}
			}
		}
		var tbody = t.appendChild(document.createElement("tbody"));
		// Content rows

		if (lomake[propt].type == "text&number"){
			for (var i=0; i<lomake[propt].slots.length; i++){
				var tr = tbody.appendChild(document.createElement("tr"));

				var th = tr.appendChild(document.createElement("th"));
				th.innerHTML = lomake[propt].slots[i];

				// for pools:
				if (lomake[propt].pools){
					var td = tr.appendChild(document.createElement("td"));
					var n = td.appendChild(document.createElement("input"));
					n.setAttribute("onchange",'changehandler("'+target("pools",propt,i)+'",this,event);');
					n.setAttribute("type","number");
                    if (lomake[propt].min != null) n.setAttribute("min", lomake[propt].min);
                    if (lomake[propt].max != null) n.setAttribute("max", lomake[propt].max);
					n.value = lomake[propt].pools[i];
				}
				var td = tr.appendChild(document.createElement("td"));

				// Normal stat
				var n = td.appendChild(document.createElement("input"));
				n.setAttribute("onchange",'changehandler("'+target("vals",propt,i)+'",this,event);');
				n.setAttribute("type","number");
                if (lomake[propt].min != null) n.setAttribute("min", lomake[propt].min);
                if (lomake[propt].max != null) n.setAttribute("max", lomake[propt].max);
				n.value = vals[i];




				if (lomake[propt].tooltips){
					// console.log(propt);
					tr.title = lomake[propt].slots[i] + ": " + lomake[propt].tooltips[lomake[propt].slots[i]].explanation;
				}

			}
		} else if (lomake[propt].type == "editable text&number"){
			for (var i=0; i<lomake[propt].slots.length; i++){
                //TODO: Similar code is used at else if lomake[propt].type = "select", this probably should be a function!
                var createnewrow = true;
                if (lomake[propt].columns != null && lomake[propt].columns > 1)
                {
                    var columnmodulo = i % lomake[propt].columns;
                    if (columnmodulo > 0) createnewrow = false;
                }

                if (createnewrow == true)
                {
                    var tr = tbody.appendChild(document.createElement("tr"));
                    tr.className = "editabletextandnumber-tr";
                    var td = tr.appendChild(document.createElement("td"));
                    td.className = "editabletextandnumber-td";
                } else {
                    var td = tr.appendChild(document.createElement("td"));
                    td.className = "editabletextandnumber-td";
                }

				var n = td.appendChild(document.createElement("input"));
				n.setAttribute("onchange",'changehandler("'+target("slots",propt,i)+'",this,event);');
				n.setAttribute("onkeyup",'textchangehandler("'+target("slots",propt,i)+'",this,event);');
				n.value = lomake[propt].slots[i];
				n.className = "text";


				var td = tr.appendChild(document.createElement("td"));
				var n = td.appendChild(document.createElement("input"));
				n.setAttribute("onchange",'changehandler("'+target("vals",propt,i)+'",this,event);');
				n.setAttribute("type","number");
				n.value = vals[i];

			}
		} else if (lomake[propt].type == "onlytext"){
			for (var i=0; i<lomake[propt].slots.length; i++){
                //TODO: Similar code is used at else if lomake[propt].type = "select", this probably should be a function!

                var createnewrow = true;
                if (lomake[propt].columns != null && lomake[propt].columns > 1)
                {
                    var columnmodulo = i % lomake[propt].columns;
                    if (columnmodulo > 0) createnewrow = false;
                }

                if (createnewrow == true)
                {
                    var tr = tbody.appendChild(document.createElement("tr"));
                    tr.className = "onlytext-tr";
                    var td = tr.appendChild(document.createElement("td"));
                    td.className = "onlytext-td";
                } else {
                    var td = tr.appendChild(document.createElement("td"));
                    td.className = "onlytext-td";
                }

                var n = td.appendChild(document.createElement("input"));
                n.style.width = "100%"; //has to override css :/
                n.setAttribute("onchange",'changehandler("'+target("vals",propt,i)+'",this,event);');
                n.setAttribute("onkeyup",'textchangehandler("'+target("vals",propt,i)+'",this,event);');
                if (vals[i]){
                    n.value = lomake[propt].vals[i];
                }
                n.className = "text";
			}
		} else if (lomake[propt].type == "scale"){
			// esim. harmonia
			tdhead.setAttribute("colspan","5");
			var tr = tbody.appendChild(document.createElement("tr"));
			for (var i=0; i<lomake[propt].slots.length; i++){
				var td = tr.appendChild(document.createElement("td"));

				td.innerHTML = lomake[propt].slots[i];
			}
			var tr = tbody.appendChild(document.createElement("tr"));
			for (var i=0; i<lomake[propt].slots.length; i++){
				var td = tr.appendChild(document.createElement("td"));

				// td.innerHTML = lomake[propt].slots[i];
				var c = td.appendChild(document.createElement("input"));
				c.type = "checkbox";
				c.className = "grid";
				c.setAttribute("onchange",'changehandler("'+target("vals",propt,i)+'",this,event);');

				if (lomake[propt].vals[i] == "1"){
					c.setAttribute("checked","checked");
					// console.log("ekng");
				} else {
					c.removeAttribute("checked");
				}
			}
		} else if (lomake[propt].type == "grid"){
			// kestopisteet esim
			// tdhead.setAttribute("colspan","5");
			var total = parseInt(lomake[propt].calculated[0]);
			var height = Math.ceil(total/5);


			for (var i=0; i<height; i++){
				var tr = tbody.appendChild(document.createElement("tr"));

				for (var j=0; j<=4; j++){

					var td = tr.appendChild(document.createElement("td"));

					var c = td.appendChild(document.createElement("input"));
					c.type = "checkbox";
					c.className = "grid";
					// c.setAttribute("data-gridno",i+1);
					c.setAttribute("onchange",'changehandler("'+target("vals",propt,i*5+j+1)+'",this,event);');
					if (i*5+j < lomake[propt].vals[0]){
						c.setAttribute("checked","checked");
						// console.log("ekng");
					} else {
						c.removeAttribute("checked");
					}
				}
				var td = tr.appendChild(document.createElement("td"));
				td.innerHTML = "-"+i;
			}
		} else if (lomake[propt].type == "select&number"){ // e.g. B204's spell effects and qualities
			// with maxlevels possible levels, show numberbox, name, explanation
			for (var i=0; i<lomake[propt].slots.length; i++){
				var val = lomake[propt].vals[i];
				var tr = tbody.appendChild(document.createElement("tr"));
				tr.className = "select-tr";
				// number td
				var ntd = tr.appendChild(document.createElement("td"));
				ntd.className = "select-td-number";


				// name td
				var vtd = tr.appendChild(document.createElement("td"));
				vtd.className = "select-td-name";
				// Is there data for this row?
				// var sel = (""+lomake[propt].vals[i]).substr(1).split(",");

				if (val != "0" && lomake[propt].selectables.hasOwnProperty(val)){
					vtd.innerHTML = lomake[propt].selectables[val].name;
					// Hover over for full text as tooltip
					tr.title = lomake[propt].selectables[val].name + ": " + lomake[propt].selectables[val].explanation;
					// Add effects as another td
					var tdt = creel("td","","select-td-expl");
					addel(tr,tdt);
					tdt.innerHTML = lomake[propt].selectables[val].explanation;

					// Fill first td with number slot
					var n = ntd.appendChild(document.createElement("input"));
					n.setAttribute("onchange",'changehandler("'+target("levels",propt,i)+'",this,event);');
					n.setAttribute("type","number");
					n.value = lomake[propt].levels[i];
					vtd.setAttribute("onclick",'makeselector("'+target("vals",propt,i)+'",this,event);');
					tdt.setAttribute("onclick",'makeselector("'+target("vals",propt,i)+'",this,event);');
				} else {
					// Empty cell styling
					vtd.innerHTML ="-";
					vtd.className = "select-td empty-td";
					var tdt = tr.appendChild(document.createElement("td"));
					tdt.className = "select-td empty-td effect-td";
					tdt.innerHTML="-";
					tr.setAttribute("onclick",'makeselector("'+target("vals",propt,i)+'",this,event);');
				}


			}

		} else if (lomake[propt].type == "select"){
			// select-option things
			// console.log("rg");
			for (var i=0; i<lomake[propt].slots.length; i++){
                var createnewrow = true;
                if (lomake[propt].columns != null && lomake[propt].columns > 1)
                {
                    var columnmodulo = i % lomake[propt].columns;
                    if (columnmodulo > 0) createnewrow = false;
                }

                if (createnewrow == true)
                {
                    var tr = tbody.appendChild(document.createElement("tr"));
                    tr.className = "select-tr";
                    var td = tr.appendChild(document.createElement("td"));
                    td.className = "select-td-name";
                } else {
                    var td = tr.appendChild(document.createElement("td"));
                    td.className = "select-td-name";
                }

				// Is there data for this row?
				// var sel = (""+lomake[propt].vals[i]).substr(1).split(",");
				var val = lomake[propt].vals[i];
				if (val != "0" && lomake[propt].selectables.hasOwnProperty(val)){
					td.innerHTML = lomake[propt].selectables[val].name;
					// Hover over for full text as tooltip
                    if (lomake[propt].columns != null && lomake[propt].columns > 1)
					    td.title = lomake[propt].selectables[val].name + ": " + lomake[propt].selectables[val].explanation;
                    else
                    {
                        tr.title = lomake[propt].selectables[val].name + ": " + lomake[propt].selectables[val].explanation;
                        if (lomake[propt].writeexplanation != null && lomake[propt].writeexplanation == true)
                        {
                            // Add effects as another td
                            var tdt = creel("td","","select-td-expl");
                            addel(tr,tdt);
                            tdt.innerHTML = lomake[propt].selectables[val].explanation;
                        }
                    }
				} else {
					// Empty cell styling
					td.innerHTML ="-";
					td.className = "select-td empty-td";
                    if (lomake[propt].writeexplanation != null && lomake[propt].writeexplanation == true)
                    {
					    var tdt = tr.appendChild(document.createElement("td"));
					    tdt.className = "select-td empty-td effect-td";
					    tdt.innerHTML="-";
                    }
				}

                if (lomake[propt].columns != null && lomake[propt].columns > 1)
                {
                    td.style.border = "1px #ddd solid";
                }

                if (lomake[propt].columns != null && lomake[propt].columns > 1)
				    td.setAttribute("onclick",'makeselector("'+target("vals",propt,i)+'",this,event);');
                else
                    tr.setAttribute("onclick",'makeselector("'+target("vals",propt,i)+'",this,event);');
			}



		} else if (lomake[propt].type == "exp"){
			// Special calculations here for experience points
			for (var i=0; i<lomake[propt].slots.length; i++){
				var tr = tbody.appendChild(document.createElement("tr"));

				var th = tr.appendChild(document.createElement("th"));
				th.innerHTML = lomake[propt].slots[i];

				var td = tr.appendChild(document.createElement("td"));

				var n = td.appendChild(document.createElement("input"));
                if (lomake[propt].min != null) n.setAttribute("min", lomake[propt].min);
                if (lomake[propt].max != null) n.setAttribute("min", lomake[propt].max);
				n.value = vals[i];
				n.setAttribute("onchange",'changehandler("'+target("vals",propt,i)+'",this,event);');
				n.setAttribute("type","number");

			}
			var tr = tbody.appendChild(document.createElement("tr"));

			var th = tr.appendChild(document.createElement("th"));
			th.innerHTML = "Used";

			var td = tr.appendChild(document.createElement("td"));

			var n = td.appendChild(document.createElement("span"));
			n.innerHTML = calculate_cost();

			var tr = tbody.appendChild(document.createElement("tr"));

			var th = tr.appendChild(document.createElement("th"));
			th.innerHTML = "Available";

			var td = tr.appendChild(document.createElement("td"));

			var n = td.appendChild(document.createElement("span"));
			n.innerHTML = (parseInt(lomake.exp.vals[0])+parseInt(lomake.exp.vals[1]))- parseInt(calculate_cost());

		}
		}
	}

	// Create link to Uoti's spell generator with values from this sheet
	spellgenlink();
}

function show_advanced_options(el) {
    el.setAttribute("onclick","hide_advanced_options(this)");
    var box = creel("div","advancedoptionsbox","advancedoptionsbox","","",true);

    var close_options_button = creel("button", "advancedoptionsbox_close");
	close_options_button.setAttribute("onclick","hide_advanced_options(getelid('advancedoptions'))");
	close_options_button.innerHTML = "X";

    load_advanced_options(box); // Adds html-elements of options

    var information_text = creel("p");
    information_text.innerHTML = "<br><br><i>NPC-mode: Creates generic characters with preset amounts of attributes and experience points.</i><br><br><i>Max random perks: Maximum number of randomized perks other than those related to the concept, religion and culture.</i>";
    addel(box, information_text);

    addel(box, close_options_button);

    var daddy = el.parentNode;
    addel(daddy, box);

	box.focus();
	box.value = '';
}

function hide_advanced_options(el){
    el.setAttribute("onclick","show_advanced_options(this)");
	delel(getelid("advancedoptionsbox"));
}

function makeselector(trgt,el,ev){
	ev.stopPropagation();
	// Make textbox at target for searching perks etc. Replace el
	// creel(tagname, id, cla, attrs, NS, del)
	// addel(to,newel)
	// Grab value from lomake data
	var targt = trgt.split("_");
	// var sels = lomake[targt[1]].selectables;
	var val = lomake[targt[1]].vals[targt[2]];
	if (val == "0"){val = "";}
	// console.log("old value: ", val);
	delchildren(el);
	var searchbox = creel("input","","select-searchbox",["type","text","onkeyup","selector_search('"+trgt+"',this,event)"],"",true);
	addel(el, searchbox);
	// Also already bring up the search result window
	selector_search(trgt,searchbox,ev);
	// Focus on text input and force cursor to last character
	searchbox.focus();
	searchbox.value = '';
	searchbox.value = val;

}
function selector_search(trgt,el,ev){
	// Show results of selectable search
	var searchstring = el.value.toLowerCase();
	// console.log("selector_search");

	var box = creel("div","search_result_box","search_result_box",[],"",true);
	// box.innerHTML = "dick";
	addelafter(el, box);
	// Search in selectables
	var targt = trgt.split("_");
	var sels = lomake[targt[1]].selectables;
	// console.log("selectables: ", sels);
	var inname = [];
	var inexpl = [];

	var selprops = Object.getOwnPropertyNames(sels);
	for (var i=0; i<selprops.length; i++){
		var name = selprops[i].toLowerCase();
		if (name.indexOf(searchstring) > -1){
			inname.push(sels[selprops[i]]);
		}



	}

	// console.log(inname);
	var t = creel("table");
	addel(box, t);

	// First option is to remove perk
	var tr = creel("tr");
	addel(t,tr);
	tr.setAttribute("onclick","select_search_result('"+trgt+"', '')");
	var td = creel("td");
	addel(tr,td);
	td.innerHTML = "(Remove)";

	// Render search results
	// TODO: Arrange by type: physical, magic...
	var types = false;
	if (inname[0] && inname[0].hasOwnProperty("type")){
		types = true;
		var last_type = ""
	}

	for (var i=0; i<inname.length; i++){

		if (types) {
			if (inname[i].type != last_type) {
				var tr = creel("tr");
				addel(t,tr);
				var td = creel("th");
				addel(tr,td);
				td.innerHTML = inname[i].type;
				last_type = inname[i].type;
			}
		}

		var tr = creel("tr");
		addel(t,tr);
		tr.setAttribute("onclick","select_search_result('"+trgt+"', '"+inname[i].name+"')");
		var td = creel("td");
		addel(tr,td);
		td.innerHTML = inname[i].name;
		var td = creel("td");
		addel(tr,td);
		td.innerHTML = inname[i].explanation;

	}

}
function select_search_result(trgt, name){
	var trgt = trgt.split("_");
	// console.log("closed selector");
	lomake[trgt[1]].vals[trgt[2]] = name;
	// if has per_level_cost, start at 1 level
	if (lomake[trgt[1]].per_level_cost){
		lomake[trgt[1]].levels[trgt[2]] = 1;
	}
	render_lomake();
	backup();
}

/*
// Old code for selectables
function closeselector(trgt,ij){

	var o = document.getElementById("selector_area");
	while (o.hasChildNodes()){
		o.removeChild(o.childNodes[0]);
	}
	if (trgt&&ij){
		var trgt = trgt.split("_");
		console.log("close selector");
		lomake[trgt[1]].vals[trgt[2]] = ij;
	}
	render_lomake();
	backup();
}
 */
function target(type,txt,number){
	// For understanding what properties to change on clicks
	return [type,txt,number].join("_");
}
function textchangehandler(trgt,el,ev){
	// This is activated onchange on text boxes, because otherwise clicking another element before the text is saved resets the text.
	var trgt = trgt.split("_");
	lomake[trgt[1]][trgt[0]][trgt[2]] = el.value;
	backup();
}
function changehandler(trgt,el,ev){
	ev.stopPropagation();

	// Save data when a value is changed
	// vals_kestopisteet_14
	var trgt = trgt.split("_");
	if (parseInt(el.value) <=0){
		el.value = 0;
	}

	// If value is too high, calculating would cause NaN, so limit the value
	// if(lomake[trgt[1]][trgt[0]].cost && parseInt(el.value) > lomake[trgt[1]][trgt[0]].cost.length){
		// console.log("triggered!");
		// lomake[trgt[1]][trgt[0]][trgt[2]] = lomake[trgt[1]][trgt[0]].cost.length;
	// }
	if (lomake[trgt[1]].type == "grid"){
		// esim kestopisteet
		// if checkbox is already checked, uncheck it
		if (el.checked){
			lomake[trgt[1]][trgt[0]][0] = parseInt(trgt[2]);
		} else {
			lomake[trgt[1]][trgt[0]][0] = parseInt(trgt[2]-1);
		}

	} else if (lomake[trgt[1]].type == "scale"){
		// esim harmonia
		// tyhjennä kaikki muut
		for (var i=0; i<lomake[trgt[1]][trgt[0]].length; i++){
			lomake[trgt[1]][trgt[0]][i] = "0";
		}
		lomake[trgt[1]][trgt[0]][trgt[2]] = "1";

	} else if (lomake[trgt[1]].creation_vals && trgt[0] == "vals"){

		// If special_cost, use special points first
		if ((lomake[trgt[1]].special_cost_used < lomake[trgt[1]].special_cost_pool
				|| lomake[trgt[1]].creation_vals[trgt[2]] > el.value)
				&& parseInt(el.value) <= lomake[trgt[1]].special_cost.length ){
			lomake[trgt[1]].creation_vals[trgt[2]] = parseInt(el.value);
			lomake[trgt[1]][trgt[0]][trgt[2]] = parseInt(el.value);
			// TODO: What if so many points are removed that it affects creation_vals?    maybe solved dontgeddit
			// TODO: Buying 5th lvl with 1 remaining special point causes special points to go to -1
		}  else {
			// console.log("yo");
			lomake[trgt[1]][trgt[0]][trgt[2]] = parseInt(el.value);
		}
		// var o = [];
		// for (var i=0;i<lomake[trgt[1]].creation_vals.length;i++){
			// o.push(lomake[trgt[1]].creation_vals[i]);
		// }
		// console.log(o.join(", "));
	} else if (lomake[trgt[1]].can_buy){
		// can_buy: player can buy extra points on top of calculated values
		if (el.value < lomake[trgt[1]].calculated[trgt[2]]){
			// if less than calculated value: Forbid
			lomake[trgt[1]].vals[trgt[2]] = lomake[trgt[1]].calculated[trgt[2]];
		} else {
			lomake[trgt[1]].vals[trgt[2]] = el.value;
		}


	}else {
		// Detect text vs number
		// console.log(trgt, el.value);
		val = parseInt(el.value);
		if (isNaN(val)){
			lomake[trgt[1]][trgt[0]][trgt[2]] = el.value;
		} else{
			// use number value; if is levels and exceeds maxlevels, forbid
			if (trgt[0] == "levels"){
				if (val <= parseInt(lomake[trgt[1]].selectables[ lomake[trgt[1]].vals[trgt[2]]     ].maxlevels)){

					lomake[trgt[1]][trgt[0]][trgt[2]] = val;
					// console.log(parseInt(lomake[trgt[1]].selectables[    lomake[trgt[1]].vals[trgt[2]]     ].maxlevels));
				}
			} else {
				lomake[trgt[1]][trgt[0]][trgt[2]] = val;
			}

		}
	}


	//
	render_lomake();
	backup();
	// render_lomake(); // ? if calculations have to be made?
	// TODO: if calculated, get wanted value as vals = new value - calculated
	// If calculated

}
function attr(s){
	// Get numeric value for attribute

	return parseFloat(lomake.attributes.vals[lomake.attributes.slots.indexOf(s)]);
}

function calculate_values(){
	// calc: ["(attr('Karisma')+attr('&Auml;ly'))/2",
	// "(attr('Ketteryys')+attr('Aistit'))/2",
	// "(attr('Kunto')+attr('Magiavoima'))/2"],
	// TODO: Ominaisuuksissa special_cost, pit&auml;is laskea ensin se ja sitten expot // kai tehty jo


	for (var propt in lomake){
		if (lomake[propt].calc){
			for (var i=0; i<lomake[propt].calc.length; i++){
				var calced = eval(lomake[propt].calc[i]);
				lomake[propt].calculated[i] = calced;
				// if calculated value is greater than vals, use it
				if (lomake[propt].can_buy && calced > lomake[propt].vals[i]){
					lomake[propt].vals[i] = calced;
				}
			}
		}
	}
}

function arraycost(a,c){
	var cost = 0;
	for (var i=0; i < c; i++){
		if(a[i]) {
			cost += a[i];
		} else {
			cost += a[a.length-1];
		}

	}
	return cost;
}
function calculate_cost(){
	// Calculate character total cost in exp
		// special_cost: [1,2,3,4,6],
		// creation_vals:[0,0,0,0,0,0,0], // Keeps track of the values given at character creation time, or how many special points were used before exp points are needed
		// slots: ["Voima","Ketteryys","Kunto","Aistit","Karisma","&Auml;ly","Magiavoima"],
		// special_cost_pool: 18, // Extra character creation points
		// special_cost_used: 0
	var cost = 0;
	for (var propt in lomake){
		if (lomake[propt].special_cost) {
			for (var i=0; i<lomake[propt].slots.length; i++){
				// If attributes have been bought with special points, cost is normal lvls - cost of creation_vals in normals lvls

				cost+= arraycost(lomake[propt].cost, lomake[propt].vals[i]) - arraycost(lomake[propt].cost, lomake[propt].creation_vals[i]);
				// console.log(lomake[propt].slots[i]+" costs "+arraycost(lomake[propt].cost, lomake[propt].vals[i]));
			}
		} else if (lomake[propt].cost){
			for (var i=0; i<lomake[propt].slots.length; i++){
				cost+= arraycost(lomake[propt].cost, lomake[propt].vals[i]);
				if (lomake[propt].can_buy){
					cost -= arraycost(lomake[propt].cost, lomake[propt].calculated[i]);
				}



				// console.log(lomake[propt].slots[i]+" costs "+arraycost(lomake[propt].cost, lomake[propt].vals[i]));
			}
		} else if (lomake[propt].per_cost){
			for (var i=0; i<lomake[propt].vals.length; i++){
				if (lomake[propt].vals[i] != "0" && lomake[propt].vals[i] != "" ){
					cost+= parseInt(lomake[propt].per_cost);
				}
			}
		}  else if (lomake[propt].per_level_cost){
			for (var i=0; i<lomake[propt].slots.length; i++){
				if (lomake[propt].vals[i] != "0" && lomake[propt].vals[i] != "" ){
					// console.log(propt,i , parseInt(lomake[propt].per_level_cost) * parseInt(lomake[propt].levels[i]));
					cost+= parseInt(lomake[propt].per_level_cost) * parseInt(lomake[propt].levels[i]);
				}
			}
		} else if (lomake[propt].select_cost){
			for (var i=0; i<lomake[propt].vals.length; i++){
				if (lomake[propt].vals[i] != "0" && lomake[propt].vals[i] != "" ){
					cost+= parseInt(lomake[propt].selectables[lomake[propt].vals[i]].cost);
				}
			}
		}
	}
	return cost;
}

function calculate_special_cost(){
	// Calculate character total cost in special character creation points
		// special_cost: [1,2,3,4,6],
		// creation_vals:[0,0,0,0,0,0,0], // Keeps track of the values given at character creation time, or how many special points were used before exp points are needed
		// slots: ["Voima","Ketteryys","Kunto","Aistit","Karisma","&Auml;ly","Magiavoima"],
		// special_cost_pool: 18, // Extra character creation points
		// special_cost_used: 0

	for (var propt in lomake){
		var cost = 0;
		if (lomake[propt].special_cost) {
			for (var i=0; i<lomake[propt].slots.length; i++){
				cost+= arraycost(lomake[propt].special_cost, parseInt(lomake[propt].creation_vals[i]));
				// console.log(lomake[propt].slots[i]+" costs "+arraycost(lomake[propt].cost, lomake[propt].vals[i]));
				// console.log("cost:"+cost);
			}
		}
		lomake[propt].special_cost_used = parseInt(cost);
	}

}


////////////////////////////////////////////////////////////////////////////////
function printmode(){
	// Find all inputs and change them to spans
	// TODO: grid and scale
	var els = document.getElementsByTagName("input");
	while (els.length > 0){
		// console.log("did contents: "+ els[0].value);
		var newel = document.createElement("span");
		newel.innerHTML = els[0].value;
		newel.className = "printspan";
		els[0].parentNode.replaceChild(newel,els[0]);
	}
	var b = document.getElementById("printmode");
	b.innerHTML = "Takaisin muokkaamaan";
	b.setAttribute("onclick","editmode();");
	window.print();
}
function editmode(){
	var b = document.getElementById("printmode");
	b.innerHTML = "Muuta tulostuskelpoiseksi";
	b.setAttribute("onclick","printmode();");
	render_lomake();
}
/////////////////////////////////////////////////////////////////////////////

function parse_URL(){
	// Version B203
	// Parse URL into data structure. lomake should already have .vals[] and .levels.
	if (location.hash.length > 1){
	var data=location.hash.split("-");
	// Check version
	// console.log(data[0]);
	if (data[0].replace("#","") != lomake.versio){
		alert("The character sheet data stored in the link hash does not match the current version number of the character sheet; Some of your data may be corrupted! The current version is "+ lomake.versio+", your data was from version "+data[0].replace("#","")+".");
	}

	for (var i=1; i < data.length; i++){
		var id = data[i][0];
		// console.log("Data goes to id: "+id);
		data [i] = data[i].slice(1);
		// What box does the id belong to?
		for (var propt in lomake){
			if (lomake[propt].id == id){
				// Get store method, decide how to parse
				if (lomake[propt].store == "text"){
					// console.log("target id:",id,lomake[propt].title, "store as", lomake[propt].store);
					var c = data[i].split("_");
					for (var j=0; j < c.length; j++){
						// console.log();
						lomake[propt].vals[j] = atou(c[j]);
					}
				} else if (lomake[propt].store == "both"){
					// console.log("target id:",id,lomake[propt].title, "store as", lomake[propt].store);
					var c = data[i].split("_");
					for (var j=0; j < c.length; j++){
						// console.log();
						var d = c[j].split(".");
						lomake[propt].slots[j] = atou(d[0]);
						lomake[propt].vals[j] = parseInt(alph.indexOf(d[1]));

					}
				} else if (lomake[propt].store == "store_id") {
					// console.log("target id:",id,lomake[propt].title, "store as", lomake[propt].store);
					var c = data[i].split("_");
					// console.log(c);

					for (var j=0; j < c.length; j++){
						if (data[0] == "#B202"){// Version control
							console.log("Trying to save data from old link format.");
							lomake[propt].vals[j] = atou(c[j]);
						} else {
							var d = c[j].split(".");
							if (d != ""){

								lomake[propt].slots[j] = parseInt(d[0]);
								lomake[propt].vals[j] = find_with_store_id(propt, parseInt(d[1]));

							}
						}
					}


				} else if (lomake[propt].store == "text&level") {
					// console.log("target id:",id,lomake[propt].title, "store as", lomake[propt].store);
					var c = data[i].split("_");
					for (var j=0; j < c.length; j++){
						// console.log();
						if (data[0] == "#B202"){// Version control
							// console.log("Trying to save data from old link format.");
							lomake[propt].vals[j] = atou(c[j]);
							lomake[propt].levels[j] = 1;

						} else {
							var d = c[j].split(".");
							if (d != ""){
								lomake[propt].slots[j] = parseInt(d[0]);

								lomake[propt].vals[j] = find_with_store_id(propt, parseInt(d[1]));
								lomake[propt].levels[j] = parseInt(d[2]);

							}
						}
					}
				} else if (lomake[propt].store == "special") {
					// console.log("target id:",id,lomake[propt].title, "store as", lomake[propt].store);
					var c = data[i].split(".");
					var d = c[0].split("");

					for (var j=0; j < d.length; j++){

						lomake[propt].creation_vals[j] = parseInt(alph.indexOf(d[j]));

					}
					d = c[1].split("");
					for (var j=0; j < d.length; j++){

							lomake[propt].vals[j] = parseInt(alph.indexOf(d[j]));

					}
					if (lomake[propt].pools && c[2]){
						d = c[2].split("");
						for (var j=0; j < d.length; j++){

							lomake[propt].pools[j] = parseInt(alph.indexOf(d[j]));

						}
					}
				} else if (lomake[propt].store == "large_number") {
					var c = data[i].split("_");
					for (var j=0; j < c.length; j++){
						lomake[propt].vals[j] = parseInt(c[j]);
					}

				} else {
					var c = data[i].split("");
					// console.log("else: "+c);
					for (var j=0; j < c.length; j++){
						// console.log(j+" "+c[j]);

						lomake[propt].vals[j] = parseInt(alph.indexOf(c[j]));

					}
				}
			}
		}
	}
	}/*  else { // Empty hash, initialize some values
		lomake.exp.vals[0] = 50;
	} */
}

function find_with_store_id(propt, store_id) {
	// Find perks etc (type: select) with store_id
	for (var property in lomake[propt].selectables) {
		if (lomake[propt].selectables.hasOwnProperty(property)) {
			// console.log(property);
			if (lomake[propt].selectables[property].store_id == store_id) {
				// console.log(lomake[propt].selectables[property].name);
				return lomake[propt].selectables[property].name;
			}
		}
	}
}

function backup(){
	make_URL();
}

// ucs-2 string to base64 encoded ascii
function utoa(str) {
    return window.btoa(unescape(encodeURIComponent(str))).split("=").join("");
}
// base64 encoded ascii to ucs-2 string
function atou(str) {
	// console.log("from base64: "+str)
	try{
		return decodeURIComponent(escape(window.atob(str)));
	} catch(err) {
		return "";
	}

}
function spellgenlink(){
	// return a link to Bliaron spell generator with magic stats
	var oms = "o:" + lomake.attributes.vals.join("_");
	var taid = "-t:" + lomake.magicskills.vals.join("_");
	var uri = "http://frozen.northernrealms.net/Bliaron/spellbuilder/index.html#"
	var lnk = uri+oms+taid;
	// console.log(lnk);
	document.getElementById("spellgenlink").setAttribute("href",lnk);
	bob = "#"+oms+taid;
}

function make_URL(){
	// Note: Because of using alph[], only numbers up to 61 can be saved for regular number slots. If you need values of arbitrary size, use store: "large_number" in your lomake definition.
	// TODO: Add large_number functionality :)
	// Save all data into the URL
	// versio
	var data = [];

	data.push(lomake.versio);
	for (var propt in lomake){
		var c = "";

		if (lomake[propt].slots){
			if (lomake[propt].store == "no"){
				// continue;
			} else if (lomake[propt].store == "text"){
				// Only text. if empty, do not include in output
				c = [];
				for(var i = 0; i < lomake[propt].slots.length; i++){
					if (lomake[propt].vals[i] == ""){
						c.push("");
					} else {
						c.push(utoa(lomake[propt].vals[i]));
					}

				}
				c = c.join("_");

			} else if (lomake[propt].store == "both") {
				c = [];
				for(var i = 0; i < lomake[propt].slots.length; i++){

					c.push(utoa(lomake[propt].slots[i])+"."+alph[lomake[propt].vals[i]]);

				}
				c = c.join("_");

			} else if (lomake[propt].store == "store_id") {
				c = [];
				// perks: Store slot number, index in selectables
				for(var i = 0; i < lomake[propt].slots.length; i++){
					// Only store if has data
					if (lomake[propt].vals[i] != "" && lomake[propt].selectables[lomake[propt].vals[i]]){
						c.push(
							lomake[propt].slots[i]+"."+
							lomake[propt].selectables[lomake[propt].vals[i]].store_id
						);
					}

				}
				c = c.join("_");
			} else if (lomake[propt].store == "text&level") {
				c = [];
				// Spell effects and qualitys: Store slot number, index in selectables, level number
				for(var i = 0; i < lomake[propt].slots.length; i++){
					// Only store if has data
					if (lomake[propt].vals[i] != "" && lomake[propt].selectables[lomake[propt].vals[i]]){
						c.push(
							lomake[propt].slots[i]+"."+
							lomake[propt].selectables[lomake[propt].vals[i]].store_id +"."+
							lomake[propt].levels[i]
						);
					}

				}
				c = c.join("_");

			} else if (lomake[propt].store == "special") {
				for(var i = 0; i < lomake[propt].slots.length; i++){

					c += ""+ alph[lomake[propt].creation_vals[i]];

				}
				c+= "."
				for(var i = 0; i < lomake[propt].slots.length; i++){
					c += ""+ alph[lomake[propt].vals[i]];
				}
				c+= "."
				if (lomake[propt].pools){ // pools
					for(var i = 0; i < lomake[propt].pools.length; i++){
						c += ""+ alph[lomake[propt].pools[i]];
					}
				}

			} else if (lomake[propt].store == "large_number"){ // large number, over 61
				c = [];
				for(var i = 0; i < lomake[propt].slots.length; i++){
					c.push(lomake[propt].vals[i] );

				}
				c = c.join("_");

			} else { // number under 61
				for(var i = 0; i < lomake[propt].slots.length; i++){
					c += ""+ alph[lomake[propt].vals[i]];

				}
			}
			data.push(lomake[propt].id+c);
		}
	}
	data = data.join("-");
	// console.log(data);
	location.hash=data;
}

function add_value_holders(){
	// Also resets all vals
	// Add a place to store values in the lomake data structure
	for (var propt in lomake){
		// console.log(propt);
		if (lomake[propt].slots){
			lomake[propt].vals = [];
			if (lomake[propt].type == "select&number") {lomake[propt].levels = [];}

			for(var i = 0; i < lomake[propt].slots.length; i++) {
				if (lomake[propt].type == "editable"){
					lomake[propt].vals.push("");
				} else if (lomake[propt].type == "select&number"){
					lomake[propt].vals.push("");
					lomake[propt].levels.push(0);
				} else {
					lomake[propt].vals.push(0);
					if (lomake[propt].calculated){
						lomake[propt].calculated.push(0);
					}

				}

			}
		}


	}
	lomake.exp.vals[0] = parseInt(lomake.exp.give_exp);
}

function show_name_gen(el){
	el.innerHTML = "Re-roll names";
	// Open name generator box
	// var daddy = getelid("lomake_area");
	var daddy = el.parentNode;
	var box = creel("div", "namegenbox","","","",true);
	// tagname, id, cla, attrs, NS, del)
	addel(daddy,box);
	var names = get_names (10); // ["name1", ...]
	// information and close
	var header = creel("div", "namegenbox_header");
	var info = creel("span");
	info.innerHTML = "Click to use a name:";
	var clbutton = creel("button", "namegenbox_close");
	clbutton.innerHTML = "X";
	clbutton.setAttribute("onclick","hide_name_gen()");

	addel(box,header);
	addel(header,info);
	addel(header,clbutton);

	// List
	var list = creel("ul", "namelist");
	addel(box, list);
	for (var i=0; i<names.length; i++){
		var li = creel("li", "namelist_"+i);
		li.innerHTML = names[i];
		li.setAttribute("onclick", "use_gen_name(this);");
		addel(list,li);
	}
}

function use_gen_name(el) {
	// Put name into character name field and save sheet
	lomake.playerinformation.vals[0] = el.innerHTML;
	hide_name_gen();
	render_lomake();
	backup();
}

function hide_name_gen(){
	getelid("namegenbutton").innerHTML = "Name Generator";
	delel(getelid("namegenbox"));
}

function delete_data(){
	location.hash='';
	location.reload();
}

//////////////////////////////////////////////////
function init(){
	add_value_holders();
	parse_URL();
	render_lomake();
    init_concept_list();
}

window.onload=init;


var bob = "";

function parsespellgen(){
	// Käytä tätä niin saat # -osion linkistä
	var hash = location.hash.replace("#","");
	// var hash = bob.replace("#","");
	console.log(hash);
	if (hash.length > 1){ // Jos on hash
		var data=hash.split("-");
		console.log(data);
		// Ominaisuudet (stringeinä) samassa järjestyksessä kuin hahmolomakkeessa:
		var oms = data[0].replace("o:","").split("_")
		// Taidot:
		var taid = data[1].replace("t:","").split("_")

		console.log(oms,taid);

	}
}
