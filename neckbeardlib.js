/////////////////////////////////////////////////////////////
// A small library with simple DOM manipulation quickhand functions for a more humane javascript experience by Lauri Niskanen.
// LGPL-3.0-or-later
/////////////////////////////////////////////////////////////
function getelid(id) {
	if (id){
		return document.getElementById(id);
	} else {
		return false;
	}
}

function delelid(elid) {
	var d = document.getElementById(elid);
	delel(d);
}

function delel(el) {
	if (el){el.parentNode.removeChild(el);}
}

function addel(to,newel) {
	to.appendChild(newel);
}

function addelafter(to,newel) {
	to.parentNode.insertBefore(newel, to.nextSibling);
}

function addelbefore(to,newel) {
	to.parentNode.insertBefore(newel, to);
}

function delchildren(el) {
	while (el.children.length>0){
		el.removeChild(el.children[0]);
	}
}

function creel(tagname, id, cla, attrs, NS, del) {
	var exists = getelid(id);
	if (exists){
		var n = exists;
		if (del){
			delchildren(n);
		}
	} else {
		if (NS){
			var n = document.createElementNS(NS, tagname);
		} else {
			var n = document.createElement(tagname);
		}
		if (id){ n.id = id;}
	}
	if (cla) {n.className=cla;}
	if (attrs){
		for (var i=0; i<attrs.length; i=i+2){
			// console.log(tagname,id,cla,attrs[i],attrs[i+1]);
			// if (attrs[i] == "cx"){
				// console.log(id, attrs[i+1]);
			// }
			n.setAttribute(attrs[i],attrs[i+1]);
		}
	}
	return n;
}

function creel_empty(tagname, id, cla, attrs, NS) {
	// Create element, or use existing one if it exists already, but delete its contents
	if (NS){
		var n = document.createElementNS(NS, tagname);
	} else {
		var n = document.createElement(tagname);
	}
	return creel(tagname, id, cla, attrs, NS, true);
}

function rand_int(min, max) {
    return Math.floor(Math.random()*(max-min+1)+min);
}
