/*bliaron_hahmolomake lomake model*/
// LGPL-3.0-or-later

var lomake = {
	versio: "B206",
	playerinformation: {
		type: "editable",
		store: "text", // Method for storing data in the URL hash
		id : "1", // single character id for this area, necessary for storing data in URL because javascirpt doesn't guarantee object property order
		slots: ["Name","Chronicle","Concept","Age","Culture","Religion"]
	},
	attributes: {
		title: "Attributes",
		type: "text&number",
		id : "2",
		store: "special",
		cost: [2,4,6,8,10,12,14,16,18,20], // Cost per level, or array, for cost per on each lvl
		special_cost: [1,1,1,1,2],
		creation_vals:[0,0,0,0,0,0,0], // Keeps track of the values given at character creation time, or how many special points were used before exp points are needed
		slots: ["Str (Dest)","Dex (Kine)","Sta (Life)","Cha (Heat)","Int (Thou)","Wil (Matt)","Sen (Sens)"],
		special_cost_pool: 18, // Extra character creation points
		special_cost_used: 0,
		pools: [0,0,0,0,0,0,0], // Spendable and replenishable points
        min: 0,
        max: 5
	},
	magicskills: {
		title: "Magic skills",
		type: "text&number",
		id : "3",
		cost: [1,2,3,4,5,6,7,8,9,10],
		slots: ["Spellcasting","Runesmithing","Ritualism"],
        min: 0,
        max: 10
	},
	perks: {
		title: "Perks",
		type: "select",
		store: "store_id", // store_id exists in selectables, no need to store whole text
		id : "4",
		select_cost: true,
		slots: [1,2,3,4,5], // slot names are not shown for this type, but need identifiers anyway i guess
		selectables: perks, // .explanation also used as tooltips
        writeexplanation: true
	},
	effectsandqualities: {
		title: "Mastered Effects and Qualities",
		type: "select",
		store: "store_id",
		id : "9",
		select_cost: true,
		slots: [1,2,3,4,5,6,7,8], // slot names are not shown for this type, but need identifiers anyway i guess
		selectables: effectsandqualities,
        columns: 2
	},
	skills: {
		// Levels 1-2: 1
		// Levels 3-4: 3
		// Levels 5  : 5
		// Levels 6-8: 6
		title: "Skills",
		type: "text&number",
		id : "5",
		cost: [1,2,3,4,5,6,7,8,9,10],
		slots: ["Animal handling","Athletics","Crafts","Intimidation","Lore","Medicine","Melee","Negotiation","Notice","Ranged","Seafaring","Seduction","Sleight of Hand","Stealth","Survival","Unarmed"],
		tooltips: skills,
        min: 0,
        max: 10
	},
	additionals: {
        // Additional stats facilitate role-playing
        // Default value for Wealth = 2
        // Status (in development, may not be needed in character sheet)
        // Reputation (in development, may not be needed in character sheet)
        // Only Wealth is bound to a game mechanic
		title: "Additional stats",
		type: "text&number",
		id : "7",
		can_buy: true, // Player can buy extra points, vals then represents the total of calculated+bought
		cost: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
		// calc: Write valid javascript. attr() gets value for an attribute, Math.floor, .ceil & .round can be used too
		// calc: [
		//	"Math.round((attr('Ketteryys / Liike')+attr('Aistit / Aistit'))/2)",
		//	"Math.round((attr('Kunto / El&auml;m&auml;')+attr('Tahdonvoima / Materia'))/2)"],
		slots: ["Wealth", "Harmony","Reputation"],
		calculated: [] // Calculated values
	},
	hitpoints:{
		title: "Hitpoints",
		type: "grid",
		id : "8",
		slots: ["hp"],
		calc: ["attr('Sta (Life)')*5+10 +5*has_perk('Huge (+5)') -5*has_perk('Fragile (-4)') "],
		calculated: []
	},
	equipment:{
		title: "Equipment",
		type: "onlytext",
		store: "text",
		id : "a",
		slots: [1,2,3,4,5,6,7,8,9,10],
        columns: 2
	},
	exp:{
		title: "Experience points",
		store: "large_number",
		type: "exp",
		id : "c",
		give_exp: 65, // How much exp to give new character
		slots: ["Character Creation","Gained"],
        min: 0
	}

};
