// Pre-baked information about perks, spell effects and such. Based on csv/perks.csv etc. baked by running bake_perks.php

var effectsandqualities = {
	"Sense matter and magic": {"name": "Sense matter and magic", "cost": "3", "attribute": "Sen (Sens)", "explanation": "Spell senses material and magic, also spells with Hidden Quality of the same or lower level", "type": "effect", "from": "", "store_id": 0},
	"Create a sensation": {"name": "Create a sensation", "cost": "3", "attribute": "Sen (Sens)", "explanation": "Spell creates a vision, a sound, or a sensation of touch. Also transparency and invisibility.", "type": "effect", "from": "", "store_id": 1},
	"Heat / Cool": {"name": "Heat / Cool", "cost": "3", "attribute": "Cha (Heat)", "explanation": "Spell creates heat or cold, may create fire or ice", "type": "effect", "from": "", "store_id": 2},
	"Change Weather": {"name": "Change Weather", "cost": "3", "attribute": "Cha (Heat)", "explanation": "Spell changes weather temperature", "type": "effect", "from": "", "store_id": 3},
	"Alter emotion": {"name": "Alter emotion", "cost": "3", "attribute": "Cha (Heat)", "explanation": "Spell transmits an emotion. Resisted with Charisma vs 6 + 2 per level.", "type": "effect", "from": "", "store_id": 4},
	"Create a force": {"name": "Create a force", "cost": "3", "attribute": "Dex (Kine)", "explanation": "Spell creates a force that moves matter and magical energy.", "type": "effect", "from": "", "store_id": 5},
	"Change shape": {"name": "Change shape", "cost": "3", "attribute": "Wil (Matt)", "explanation": "Spell changes the shape of a target made of certain material type.", "type": "effect", "from": "", "store_id": 6},
	"Change material": {"name": "Change material", "cost": "3", "attribute": "Wil (Matt)", "explanation": "Spell changes the material of the target.", "type": "effect", "from": "", "store_id": 7},
	"Destroy flesh / matter": {"name": "Destroy flesh / matter", "cost": "3", "attribute": "Str (Dest)", "explanation": "Spell destroys whatever it hits.", "type": "effect", "from": "", "store_id": 8},
	"Banish source": {"name": "Banish source", "cost": "3", "attribute": "Str (Dest)", "explanation": "Spell decreases Source that can be gathered with Gather Source -action", "type": "effect", "from": "", "store_id": 9},
	"Spellshield": {"name": "Spellshield", "cost": "3", "attribute": "Str (Dest)", "explanation": "Spell removes Effect or Quality levels from targeted spell.", "type": "effect", "from": "", "store_id": 10},
	"Heal": {"name": "Heal", "cost": "3", "attribute": "Sta (Life)", "explanation": "Spell heals injuries and sicknesses.", "type": "effect", "from": "", "store_id": 11},
	"Create life": {"name": "Create life", "cost": "3", "attribute": "Sta (Life)", "explanation": "Spell creates life out of pure magic. May be used to create new body parts.", "type": "effect", "from": "", "store_id": 12},
	"Create disease": {"name": "Create disease", "cost": "3", "attribute": "Sta (Life)", "explanation": "Creates a disease that lowers a chosen attribute. Renews every day unless cured with Stamina vs 7 + level.", "type": "effect", "from": "", "store_id": 13},
	"Read mind": {"name": "Read mind", "cost": "3", "attribute": "Int (Thou)", "explanation": "Spell reads thoughts of the target.", "type": "effect", "from": "", "store_id": 14},
	"Alter mind": {"name": "Alter mind", "cost": "3", "attribute": "Int (Thou)", "explanation": "Spell may transmit or alter the mind of the target. Resisted with Willpower vs 6 + 2 per level.", "type": "effect", "from": "", "store_id": 15},
	"Hidden": {"name": "Hidden", "cost": "3", "attribute": "None", "explanation": "Hiding a spell from plain view.", "type": "quality", "from": "", "store_id": 16},
	"Precise": {"name": "Precise", "cost": "3", "attribute": "None", "explanation": "Focusing a spell into precise, smaller area. Defining the details.", "type": "quality", "from": "", "store_id": 17},
	"Moving": {"name": "Moving", "cost": "3", "attribute": "None", "explanation": "Making a spell move or changing its direction.", "type": "quality", "from": "", "store_id": 18},
	"Teleported": {"name": "Teleported", "cost": "3", "attribute": "None", "explanation": "Teleporting the spell effect to another place.", "type": "quality", "from": "", "store_id": 19},
	"Area": {"name": "Area", "cost": "3", "attribute": "None", "explanation": "Creating big spells, covering wider areas.", "type": "quality", "from": "", "store_id": 20},
	"Split": {"name": "Split", "cost": "3", "attribute": "None", "explanation": "Splitting single spells into smaller segments.", "type": "quality", "from": "", "store_id": 21},
	"Duration": {"name": "Duration", "cost": "3", "attribute": "None", "explanation": "Creating longer lasting spells.", "type": "quality", "from": "", "store_id": 22},
	"Conditional": {"name": "Conditional", "cost": "3", "attribute": "None", "explanation": "Giving spells conditions when they should fire (not for spellcasting).", "type": "quality", "from": "", "store_id": 23}
};


var historicalevents = {
	"697": {"name": "697", "event": "The trade and military union of Bliwon is established", "expl": "", "affects": "", "store_id": 0},
	"812": {"name": "812", "event": "Republic of Bliwon is established", "expl": "", "affects": "", "store_id": 1},
	"902": {"name": "902", "event": "The division", "expl": "First Sahen rips apart into three competing organizations: Puvo, Thur and Volar.", "affects": "", "store_id": 2},
	"998": {"name": "998", "event": "Awakening of Artan", "expl": "Artan is awakened from his eternal sleep, and resurrected back to life through time and space.", "affects": "", "store_id": 3},
	"999": {"name": "999", "event": "Gathering of Power", "expl": "King Artan slips into city of Artan in a weak skeleton form, starts to gather pieces of his soul, Blackhand politics ensues", "affects": "Artan,Saras", "store_id": 4},
	"1000": {"name": "1000", "event": "Age of Artan begins", "expl": "King Artan opens a portal to the ancient Kalthan Palace, and designates himself as the true King, minor conflicts ensues", "affects": "Artan", "store_id": 5},
	"1001": {"name": "1001", "event": "The Conquest of Horuc begins", "expl": "Artans war against Thur in Horuc begins", "affects": "Carehos,Bomrin,Habis Criil,Gatos,Artan,Saras", "store_id": 6},
	"1001": {"name": "1001", "event": "The talks for Grand Sahen begin", "expl": "Puvo Sahen starts to actively talk of uniting the Sahen once again, few Volar fortressess immediately join the cause", "affects": "", "store_id": 7},
	"1005": {"name": "1005", "event": "Thur destroyed", "expl": "The Elemental Fortress near Carehos is conquered, signaling the end for Thur", "affects": "Carehos,Bomrin", "store_id": 8},
	"1006": {"name": "1006", "event": "The Grand Sahen is established", "expl": "Puvo claims itself as the Grand Sahen, many high ranking ex-Thur and Volar officials support the cause, gaining high positions in Grand Sahen.", "affects": "", "store_id": 9},
	"1007": {"name": "1007", "event": "Carehos officially honors King Artan", "expl": "Last opposing guerilla forces near Carehos are raided by forces of Artan. City of Carehos official bows to Artan.", "affects": "Carehos,Bomrin,Habis Criil,Gatos,Artan,Saras", "store_id": 10},
	"1008": {"name": "1008", "event": "The Conquest of Horuc ends", "expl": "Forces of Artan arrive back to city of Artan and Saras", "affects": "Artan,Saras", "store_id": 11}
};


var perks = {
	"Huge (+5)": {"name": "Huge (+5)", "cost": "5", "explanation": "+5 hit points. Can't fit in small spaces.", "type": "physical", "from": "", "adjective_child": "", "store_id": 0},
	"Iron-boned (+4)": {"name": "Iron-boned (+4)", "cost": "4", "explanation": "+2 to unarmed damage and armor, can fall 5m without an injury.", "type": "physical", "from": "", "adjective_child": "", "store_id": 1},
	"Quick healer (+3)": {"name": "Quick healer (+3)", "cost": "3", "explanation": "Healing rate is tripled.", "type": "physical", "from": "", "adjective_child": "", "store_id": 2},
	"Night sight (+2)": {"name": "Night sight (+2)", "cost": "2", "explanation": "Can see in darkness. Dark surroundings give no negative Modifiers.", "type": "physical", "from": "", "adjective_child": "", "store_id": 3},
	"Diver (+1)": {"name": "Diver (+1)", "cost": "1", "explanation": "Can dive up to 5 minutes", "type": "physical", "from": "", "adjective_child": "", "store_id": 4},
	"Quick-footed (+1)": {"name": "Quick-footed (+1)", "cost": "1", "explanation": "Gets +2 modifier to contests requiring speed.", "type": "physical", "from": "", "adjective_child": "", "store_id": 5},
	"Scarred (0)": {"name": "Scarred (0)", "cost": "0", "explanation": "May give positive Modifiers to Intimidation, but easily recognized.", "type": "physical", "from": "", "adjective_child": "", "store_id": 6},
	"Fat (-1)": {"name": "Fat (-1)", "cost": "-1", "explanation": "-2 to Athletics. No negative Modifiers of starvation (remove this perk instead).", "type": "physical", "from": "", "adjective_child": "", "store_id": 7},
	"Sickly (-1)": {"name": "Sickly (-1)", "cost": "-1", "explanation": "Requires daily treatment (e.g. herbal treatments, baths or salves) or gets a level 1 disease (see Create a Disease Effect on page 135).", "type": "physical", "from": "", "adjective_child": "", "store_id": 8},
	"Deformed (-2)": {"name": "Deformed (-2)", "cost": "-2", "explanation": "Clearly visible physical oddity that gives a Modifier of -1 for all social interactions.", "type": "physical", "from": "", "adjective_child": "", "store_id": 9},
	"Lame (-3)": {"name": "Lame (-3)", "cost": "-3", "explanation": "Dexterity is 1 when attempting physical tasks. Modifier of -2 to physical skill checks if not using a mobility aid.", "type": "physical", "from": "", "adjective_child": "", "store_id": 10},
	"Fragile (-4)": {"name": "Fragile (-4)", "cost": "-4", "explanation": "Hitpoints are calculated as [Sta * 5 + 5].", "type": "physical", "from": "", "adjective_child": "", "store_id": 11},
	"Natural affinity (+6)": {"name": "Natural affinity (+6)", "cost": "6", "explanation": "+1 to casting checks when using chosen magical Force.", "type": "magical", "from": "", "adjective_child": "", "store_id": 12},
	"Soulbound (+5)": {"name": "Soulbound (+5)", "cost": "5", "explanation": "A spirit is co-occupying the person’s body, giving +4 levels of Attributes (see also the limits of Soul-binding spell on page 143). This is considered demonistic by the Grand Sahen and is punishable by death.", "type": "magical", "from": "", "adjective_child": "", "store_id": 13},
	"Inherent level (+4)": {"name": "Inherent level (+4)", "cost": "4", "explanation": "Choose one level from a mastered Effect/Quality as inherent Effect/Quality level.", "type": "magical", "from": "", "adjective_child": "", "store_id": 14},
	"True vision (+4)": {"name": "True vision (+4)", "cost": "4", "explanation": "Sees magical streams without using Senses magic. See spells with Hidden 1.", "type": "magical", "from": "", "adjective_child": "", "store_id": 15},
	"Hero (+2)": {"name": "Hero (+2)", "cost": "2", "explanation": "Choose a skill that has a cool non-mechanical special effect that makes the actions of this character stand out.", "type": "magical", "from": "", "adjective_child": "", "store_id": 16},
	"Hidden soul (+2)": {"name": "Hidden soul (+2)", "cost": "2", "explanation": "Soulbound Hidden 1 Quality, making the Soul seem weaker than it actually is. (See Sense matter and magic on page 124 for more details.)", "type": "magical", "from": "", "adjective_child": "", "store_id": 17},
	"Sixth Sense (+2)": {"name": "Sixth Sense (+2)", "cost": "2", "explanation": "Once per day, the player may ask whether this day carries unusual risk of death. The GM must answer truthfully.", "type": "magical", "from": "", "adjective_child": "", "store_id": 18},
	"Lucky (+2)": {"name": "Lucky (+2)", "cost": "2", "explanation": "May re-roll any skillcheck once during play.", "type": "magical", "from": "", "adjective_child": "", "store_id": 19},
	"Blessed (+2)": {"name": "Blessed (+2)", "cost": "2", "explanation": "Once per game session, the player may ask GM that spirits cause a spontaneous beneficial spell to help the character.", "type": "magical", "from": "", "adjective_child": "", "store_id": 20},
	"Visions (+2)": {"name": "Visions (+2)", "cost": "2", "explanation": "Once per day, the player may ask GM for a vision of the future. The vision may be short and vague, but should reflect truth.", "type": "magical", "from": "", "adjective_child": "", "store_id": 21},
	"Blood of Ide Jem (-2)": {"name": "Blood of Ide Jem (-2)", "cost": "-2", "explanation": "The character looks like a monster when casting spells. This is likely to raise suspicion of demonism by the mages of Grand Sahen, punishable by death.", "type": "magical", "from": "bastard", "adjective_child": "", "store_id": 22},
	"Unstable Aura (-2)": {"name": "Unstable Aura (-2)", "cost": "-2", "explanation": "The GM may add random Effects or Qualities to the spells of the character. The player should remind the GM of this perk at least once during the game session.", "type": "magical", "from": "", "adjective_child": "", "store_id": 23},
	"Unlucky (-2)": {"name": "Unlucky (-2)", "cost": "-2", "explanation": "Once per game session, the GM may reroll a successful skillcheck. The player should remind the GM of this perk at least once during the game session.", "type": "magical", "from": "", "adjective_child": "", "store_id": 24},
	"Tormented (-3)": {"name": "Tormented (-3)", "cost": "-3", "explanation": "The character has a mischievous pet spirit that keeps making trouble. The player should remind the GM of this perk at least once during the game session.", "type": "magical", "from": "", "adjective_child": "", "store_id": 25},
	"Unhidden mage (-3)": {"name": "Unhidden mage (-3)", "cost": "-3", "explanation": "Person was awakened to a state where all spells are unleashed with loud and bright effects, and when spellcasting, a level of Hidden is canceled from their spells.", "type": "magical", "from": "", "adjective_child": "", "store_id": 26},
	"Cursed tattoo (-4)": {"name": "Cursed tattoo (-4)", "cost": "-4", "explanation": "Character has a tattoo that carries a curse. The spell activates once a day on a condition that severely bothers the life of the character (e.g. “when starting a melee combat” or “when speaking”). You may make your own runespell, or pick one from Magic Items (see page 210) and change it so that it’s harmful to the character.", "type": "magical", "from": "failed ritual, exile", "adjective_child": "", "store_id": 27},
	"Enemy of a Force (-5)": {"name": "Enemy of a Force (-5)", "cost": "-5", "explanation": "May not use a chosen Force in spells. Harmful Effects targeting the character are one level stronger.", "type": "magical", "from": "", "adjective_child": "", "store_id": 28},
	"Chaotic Aura (-6)": {"name": "Chaotic Aura (-6)", "cost": "-6", "explanation": "All spells cast by the character or in touch range, get a random Effect. The player must remind the GM of this perk when necessary.", "type": "magical", "from": "", "adjective_child": "", "store_id": 29},
	"Amnesia (0)": {"name": "Amnesia (0)", "cost": "0", "explanation": "Has significant gaps in memory, may have experienced a trauma.", "type": "mental", "from": "", "adjective_child": "forgetful", "store_id": 30},
	"Anarchist (0)": {"name": "Anarchist (0)", "cost": "0", "explanation": "Fights against the authorities, hates control and values individualism.", "type": "mental", "from": "", "adjective_child": "feeble", "store_id": 31},
	"Authoritarian (0)": {"name": "Authoritarian (0)", "cost": "0", "explanation": "Dwells on authority, and actively defends and builds superior social status.", "type": "mental", "from": "", "adjective_child": "obedient", "store_id": 32},
	"Attention seeker (0)": {"name": "Attention seeker (0)", "cost": "0", "explanation": "A star of his own life. Afraid to be left out.", "type": "mental", "from": "", "adjective_child": "wild", "store_id": 33},
	"Code of Honor (0)": {"name": "Code of Honor (0)", "cost": "0", "explanation": "Follows a strict code of conduct, duty and order.", "type": "mental", "from": "", "adjective_child": "obedient", "store_id": 34},
	"Cunning (0)": {"name": "Cunning (0)", "cost": "0", "explanation": "Prefers acting behind the shadows. Believes that best chance to survive is through superior wits.", "type": "mental", "from": "", "adjective_child": "mischievous", "store_id": 35},
	"Delusional (0)": {"name": "Delusional (0)", "cost": "0", "explanation": "Follows paranoid or irrational rules.", "type": "mental", "from": "", "adjective_child": "fearful", "store_id": 36},
	"Direct (0)": {"name": "Direct (0)", "cost": "0", "explanation": "Taking action immediately is the best solution.", "type": "mental", "from": "", "adjective_child": "restless", "store_id": 37},
	"Good-willed (0)": {"name": "Good-willed (0)", "cost": "0", "explanation": "Believes that people are good in the inside. Tries to find good everywhere.", "type": "mental", "from": "", "adjective_child": "lovely", "store_id": 38},
	"Idealist (0)": {"name": "Idealist (0)", "cost": "0", "explanation": "Has a strong opinion, and isn't easily swayed. Sees admitting defeat as a weakness.", "type": "mental", "from": "", "adjective_child": "idealist", "store_id": 39},
	"Impulsive (0)": {"name": "Impulsive (0)", "cost": "0", "explanation": "Acts on small triggers, doesn't think of consequences or the bigger picture.", "type": "mental", "from": "", "adjective_child": "stubborn", "store_id": 40},
	"Love sick (0)": {"name": "Love sick (0)", "cost": "0", "explanation": "Never felt true love, overwhelmed by finding one. Sees opportunities everywhere.", "type": "mental", "from": "", "adjective_child": "impulsive", "store_id": 41},
	"Narcissistic (0)": {"name": "Narcissistic (0)", "cost": "0", "explanation": "Knows the best, is the best, and superior to others. Will defend his superiority through violence and wits.", "type": "mental", "from": "", "adjective_child": "loud mouthed", "store_id": 42},
	"Obsession (0)": {"name": "Obsession (0)", "cost": "0", "explanation": "Has only one goal, and everything may be sacrificed for it. Thinks that it must be solved immediately.", "type": "mental", "from": "", "adjective_child": "kind", "store_id": 43},
	"Pacifist (0)": {"name": "Pacifist (0)", "cost": "0", "explanation": "Believes in peace and non-violent acts, values life above everything.", "type": "mental", "from": "", "adjective_child": "obsessive", "store_id": 44},
	"Passionate (0)": {"name": "Passionate (0)", "cost": "0", "explanation": "Has strong feelings, and passions. Wants to be touched and moved by life.", "type": "mental", "from": "", "adjective_child": "weak", "store_id": 45},
	"Pleasure-seeker (0)": {"name": "Pleasure-seeker (0)", "cost": "0", "explanation": "Seeks to overcome the misery of life through earthly delights and superficial pleasures.", "type": "mental", "from": "", "adjective_child": "attentive", "store_id": 46},
	"Prick (0)": {"name": "Prick (0)", "cost": "0", "explanation": "Aggressive by nature, feels the need to enforce his status and confidence through bullying others.", "type": "mental", "from": "", "adjective_child": "playful", "store_id": 47},
	"Responsible(0)": {"name": "Responsible(0)", "cost": "0", "explanation": "Tends to take responsibility, and sees that things will be done.", "type": "mental", "from": "", "adjective_child": "helpful", "store_id": 48},
	"Sadistic (0)": {"name": "Sadistic (0)", "cost": "0", "explanation": "Enjoys creating pain and hurting other people. May or may not understand ethical concerns of sadism.", "type": "mental", "from": "", "adjective_child": "mean", "store_id": 49},
	"Seducer (0)": {"name": "Seducer (0)", "cost": "0", "explanation": "Believes to be attractive and desired, and uses this as a tool. Confident in one's own sexuality.", "type": "mental", "from": "", "adjective_child": "well-mannered", "store_id": 50},
	"Servant (0)": {"name": "Servant (0)", "cost": "0", "explanation": "Wants to be accepted through labor, service and pleasing others.", "type": "mental", "from": "", "adjective_child": "inattentive", "store_id": 51},
	"Thinker (0)": {"name": "Thinker (0)", "cost": "0", "explanation": "Seeks pleasure through intellectual challenge, and threatened by boredom and idiots.", "type": "mental", "from": "", "adjective_child": "wild", "store_id": 52},
	"Threatened (0)": {"name": "Threatened (0)", "cost": "0", "explanation": "Scared of many things, easily hurt and offended.", "type": "mental", "from": "", "adjective_child": "scared", "store_id": 53},
	"Thrill-seeker (0)": {"name": "Thrill-seeker (0)", "cost": "0", "explanation": "Loves action and danger. Life is nothing without a challenge and action.", "type": "mental", "from": "", "adjective_child": "nasty", "store_id": 54},
	"Sahen 4th grade (+9)": {"name": "Sahen 4th grade (+9)", "cost": "9", "explanation": "Has a 4th grade Sahen title. Wealth 4.", "type": "background", "from": "", "adjective_child": "", "store_id": 55},
	"Noble (+8)": {"name": "Noble (+8)", "cost": "8", "explanation": "A titled noble who owns a mansion. Wealth 4.", "type": "background", "from": "", "adjective_child": "", "store_id": 56},
	"Chief (+7)": {"name": "Chief (+7)", "cost": "7", "explanation": "Leader of a tribe by title and tradition.", "type": "background", "from": "", "adjective_child": "", "store_id": 57},
	"Sahen 3rd grade (+6)": {"name": "Sahen 3rd grade (+6)", "cost": "6", "explanation": "Has a 3rd grade Sahen title. Wealth 3.", "type": "background", "from": "", "adjective_child": "", "store_id": 58},
	"Landowner (+5)": {"name": "Landowner (+5)", "cost": "5", "explanation": "Owns a farm, or other establishment. Wealth 3", "type": "background", "from": "", "adjective_child": "", "store_id": 59},
	"Blackhand authority (+4)": {"name": "Blackhand authority (+4)", "cost": "4", "explanation": "Well-established Blackhand. Wealth 3.", "type": "background", "from": "", "adjective_child": "", "store_id": 60},
	"Blackhand hunter (+4)": {"name": "Blackhand hunter (+4)", "cost": "4", "explanation": "Karma member, decent equipment, Wealth 3.", "type": "background", "from": "", "adjective_child": "", "store_id": 61},
	"Family heirloom (+4)": {"name": "Family heirloom (+4)", "cost": "4", "explanation": "Possesses a magical item.", "type": "background", "from": "", "adjective_child": "", "store_id": 62},
	"Sahen 2nd grade (+4)": {"name": "Sahen 2nd grade (+4)", "cost": "4", "explanation": "Has a 2nd grade Sahen title. Wealth 2.", "type": "background", "from": "", "adjective_child": "", "store_id": 63},
	"Volar mage (+3)": {"name": "Volar mage (+3)", "cost": "3", "explanation": "Has a mage title in Volar.", "type": "background", "from": "", "adjective_child": "", "store_id": 64},
	"Well-off (+3)": {"name": "Well-off (+3)", "cost": "3", "explanation": "Has achieved a high standard of living. Wealth 2.", "type": "background", "from": "", "adjective_child": "", "store_id": 65},
	"Sahen 1st grade (+2)": {"name": "Sahen 1st grade (+2)", "cost": "2", "explanation": "Has a 1st grade Sahen title.", "type": "background", "from": "", "adjective_child": "", "store_id": 66},
	"Artan Blackhand (+1)": {"name": "Artan Blackhand (+1)", "cost": "1", "explanation": "Recognized as Artan family member.", "type": "background", "from": "", "adjective_child": "", "store_id": 67},
	"Cultist (+1)": {"name": "Cultist (+1)", "cost": "1", "explanation": "Member of a demon cult.", "type": "background", "from": "", "adjective_child": "", "store_id": 68},
	"Believer (+1)": {"name": "Believer (+1)", "cost": "1", "explanation": "Can ask help from a religious organization.", "type": "background", "from": "", "adjective_child": "", "store_id": 69},
	"Bliwon Blackhand (+1)": {"name": "Bliwon Blackhand (+1)", "cost": "1", "explanation": "Recognized as a Blackhand by allies in Bliwon.", "type": "background", "from": "", "adjective_child": "", "store_id": 70},
	"Helped someone (+1)": {"name": "Helped someone (+1)", "cost": "1", "explanation": "Is owed a debt of honor.", "type": "background", "from": "", "adjective_child": "", "store_id": 71},
	"Sahen novice (+1)": {"name": "Sahen novice (+1)", "cost": "1", "explanation": "Tutored by a master mage, life tightly scheduled.", "type": "background", "from": "", "adjective_child": "", "store_id": 72},
	"Tribal Sarasian (+1)": {"name": "Tribal Sarasian (+1)", "cost": "1", "explanation": "Carries the marks of a warrior tribe.", "type": "background", "from": "", "adjective_child": "", "store_id": 73},
	"Volar apprentice (+1)": {"name": "Volar apprentice (+1)", "cost": "1", "explanation": "Trusted by one Volar outpost, tutored.", "type": "background", "from": "", "adjective_child": "", "store_id": 74},
	"Member of a Tribe (0)": {"name": "Member of a Tribe (0)", "cost": "0", "explanation": "Origins from the Steppe or other tribe, despised in Civilization.", "type": "background", "from": "", "adjective_child": "", "store_id": 75},
	"Got the Call (0)": {"name": "Got the Call (0)", "cost": "0", "explanation": "Called to Bliwon Blackhand society.", "type": "background", "from": "", "adjective_child": "", "store_id": 76},
	"Bastard (-1)": {"name": "Bastard (-1)", "cost": "-1", "explanation": "Was never accepted as a legitimate child.", "type": "background", "from": "", "adjective_child": "", "store_id": 77},
	"Escaped slave (-1)": {"name": "Escaped slave (-1)", "cost": "-1", "explanation": "Someone thinks the character shouldn't be free.", "type": "background", "from": "", "adjective_child": "", "store_id": 78},
	"Investigated (-1)": {"name": "Investigated (-1)", "cost": "-1", "explanation": "Sahen or some other authority is doing an investigation on the character.", "type": "background", "from": "", "adjective_child": "", "store_id": 79},
	"Orphan (-1)": {"name": "Orphan (-1)", "cost": "-1", "explanation": "Lost a parent during early age.", "type": "background", "from": "", "adjective_child": "", "store_id": 80},
	"Owes a service (-1)": {"name": "Owes a service (-1)", "cost": "-1", "explanation": "NPC may ask for a favor that is dishonorable to refuse.", "type": "background", "from": "", "adjective_child": "", "store_id": 81},
	"Beggar (-2)": {"name": "Beggar (-2)", "cost": "-2", "explanation": "Your Wealth is 0, regardless of other perks.", "type": "background", "from": "", "adjective_child": "", "store_id": 82},
	"Dominating relative (-2)": {"name": "Dominating relative (-2)", "cost": "-2", "explanation": "Someone with close ties has power over the character.", "type": "background", "from": "", "adjective_child": "", "store_id": 83},
	"Marked (-2)": {"name": "Marked (-2)", "cost": "-2", "explanation": "Tattooed or burn-marked as an ex-criminal or deal-breaker.", "type": "background", "from": "", "adjective_child": "", "store_id": 84},
	"Outlaw (-2)": {"name": "Outlaw (-2)", "cost": "-2", "explanation": "Outlawed by the local authority.", "type": "background", "from": "", "adjective_child": "", "store_id": 85},
	"Secret mission (-2)": {"name": "Secret mission (-2)", "cost": "-2", "explanation": "Has a secret mission given by someone.", "type": "background", "from": "", "adjective_child": "", "store_id": 86},
	"Debted (-3)": {"name": "Debted (-3)", "cost": "-3", "explanation": "Someone actively works towards getting the payment.", "type": "background", "from": "", "adjective_child": "", "store_id": 87},
	"Hunted Blackhand (-3)": {"name": "Hunted Blackhand (-3)", "cost": "-3", "explanation": "Known criminal magic user in Bliwon, not yet caught.", "type": "background", "from": "", "adjective_child": "", "store_id": 88},
	"Blood enemy (-4)": {"name": "Blood enemy (-4)", "cost": "-4", "explanation": "Debt that can only be paid by blood.", "type": "background", "from": "", "adjective_child": "", "store_id": 89},
	"Slave (-4)": {"name": "Slave (-4)", "cost": "-4", "explanation": "Wealth 0, enslaved in the very beginning of the game.", "type": "background", "from": "", "adjective_child": "", "store_id": 90},
	"Marked as Blackhand (-5)": {"name": "Marked as Blackhand (-5)", "cost": "-5", "explanation": "Tattooed as a dangerous blackhand, actively hunted.", "type": "background", "from": "", "adjective_child": "", "store_id": 91}
};


var skills = {
	"Animal handling": {"name": "Animal handling", "explanation": "All feats involving animals, e.g. riding, training or calming and animal.", "store_id": 0},
	"Athletics": {"name": "Athletics", "explanation": "Physical movement, e.g.  acrobatics, climbing or dodging.", "store_id": 1},
	"Crafts": {"name": "Crafts", "explanation": "Creating and fixing things, carpentry, smithing etc.", "store_id": 2},
	"Intimidation": {"name": "Intimidation", "explanation": "Getting what you want by using threats.", "store_id": 3},
	"Lore": {"name": "Lore", "explanation": "Lore of history, geography, politics or other.", "store_id": 4},
	"Medicine": {"name": "Medicine", "explanation": "Treating wounds without magic.", "store_id": 5},
	"Melee": {"name": "Melee", "explanation": "Combat with close range weapons (swords, spears etc.).", "store_id": 6},
	"Negotiation": {"name": "Negotiation", "explanation": "Trading and finding good deals.", "store_id": 7},
	"Notice": {"name": "Notice", "explanation": "Finding or noticing things.", "store_id": 8},
	"Ranged": {"name": "Ranged", "explanation": "Using bows, crossbows, and throwed weapons.", "store_id": 9},
	"Ritualism": {"name": "Ritualism", "explanation": "Talking to spirits, creating magical rituals", "store_id": 10},
	"Runesmithing": {"name": "Runesmithing", "explanation": "Writing and carving magical runes.", "store_id": 11},
	"Seafaring": {"name": "Seafaring", "explanation": "Navigating at the sea, knowledge of boats and ships, fishing", "store_id": 12},
	"Seduction": {"name": "Seduction", "explanation": "Using charm to ovecome social difficulties.", "store_id": 13},
	"Sleight of Hand": {"name": "Sleight of Hand", "explanation": "Pickpocketing, hiding small items.", "store_id": 14},
	"Spellcasting": {"name": "Spellcasting", "explanation": "Manipulating magical forces.", "store_id": 15},
	"Stealth": {"name": "Stealth", "explanation": "Hiding from sight, staying hidden.", "store_id": 16},
	"Survival": {"name": "Survival", "explanation": "Navigating on land, foraging, staying warm, making shelters.", "store_id": 17},
	"Unarmed": {"name": "Unarmed", "explanation": "Wrestling and martial arts.", "store_id": 18}
};


