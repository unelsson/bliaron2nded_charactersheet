// Character history generator
// LGPL-3.0-or-later


// Find mental perks for later use
var perknames = Object.getOwnPropertyNames(perks);
var mentalperks = {};
for (var i=0; i<perknames.length; i++){
	if (perks[perknames[i]].type == "mental"){
		// Trim (0) from perk name, because they are listed like that in concepts
		var trimmed = perknames[i].substr(0,perknames[i].indexOf("(")).trimRight();
		mentalperks[trimmed] = perknames[i];
	}
}

function History (concept){

	// Create character history based on character sheet numeric values
	// i.e. find reasons for why the character has those values
	var startyear = 1008; // Game starts
	var age = getvalue("playerinformation","Age");//rand_int(16,46); // Character age during startyear
	var birthyear = startyear-age;
	var curyear = birthyear;
	var birthplace = pick_from_array (placenames);
	var location = birthplace; // Keeps track of where the character is
	var socialstanding = "";
	var name = getvalue("playerinformation","Character name");
	console.log("Creating character history for", name);
	var fname = name.substr(0, name.indexOf(" "));
	var effects = []; // Array of effects, like skills and perks, gained from life events
	var story = []; // Array that contains character's story in text snippets. Should there be an easily serializable object too? Later joined into a text with ". ".
	var events = [];
	function Event(name,year,text) {
		// Class for life events, for putting into events[]
		this.name = name;
		this.text = text;
		this.year = year;
		return this;
	}
	// Randomize character concept
	// Find a random mental perk to use based on concepts.
	var wealth = getvalue("additionals","Wealth");

	concept = concept || pick_from_array(concepts[getvalue("additionals","Wealth")]);
	console.log(concept, wealth);


	var mental = pick_from_array(concept.mental);
	lomake.perks.vals[0] = mentalperks[mental];
	console.log("mental",mental,mentalperks[mental]);
	var adjective_child = perks[mentalperks[mental]]["adjective_child"];
	console.log("adjective_child",adjective_child);

	// Wanderlust 0-1: How much the character wants to move around voluntarily
	var wanderlust = Math.random();

	// character's way of thinking randomized from mental perks
	// archetype --> suitable mental perks, skills, other perks
	// Birthplace --> culture, religion (events can change religion)
	// Parents random occupation from concept list too maybe

	// Starting money and social standing from family
	// What was the character trying to do? Apprenticeships, traveling, marriage...
	// What happened to the character during recent important historical events --> trauma, learned skills
	// Smaller random life changing events --> skills, trauma,
	// How did the character have to react to events? --> Traveling, --> trying to do...
	// Concept --> when did the character start training for their current "profession"?
	// Random time between nonhistorical events
	// Religion and culture. From childhood or later critical event.
	// Keep track of status and reputation with those slots in lomake.additionals
	// wealthdelta should direct choice of events

	// Character is born. Wealth & occupation of family.
	var birthplacewealth = places[birthplace].wealth;
	console.log("birthplacewealth",birthplacewealth);
	var family_wealth = clamp(rand_int(0, birthplacewealth),0,4);
	var family_occupation = pick_from_array(occupations[family_wealth]).name.toLowerCase();
	if (family_occupation.indexOf("(") > 0){
		family_occupation = family_occupation.substr(0,family_occupation.indexOf("(")).trimRight();
	}

	var wealth_adjectives = ["poor","hard-working","proud","wealthy","rich"];
	var family_desc = wealth_adjectives[family_wealth];

	events.push(new Event("birth", birthyear, name+", "+an(concept.name.toLowerCase())+", was born in "+birthplace+" in the year "+birthyear+" to "+an(family_desc)+" family of "+plural(family_occupation)));

	// Description of early childhood (Can result in skills or perks)
	curyear += 10;
	var aw = clamp(family_wealth + rand_int(-1,1),0,4);
	var childhood_a = pick_from_array(childhood_activities[aw]);
	var childhood_a2 = childhood_a;
	while (childhood_a.text == childhood_a2.text){ // Don't allow two that are the same
		aw = clamp(family_wealth + rand_int(-1,1),0,4); // Allow variance of +-1 in wealth, but limit to 0-4
		childhood_a2 = pick_from_array(childhood_activities[aw]);
	}
	effects.push(childhood_a.effects); // Store received perks and skills to be applied later
	effects.push(childhood_a2.effects);

	events.push(new Event("childhood",curyear, an(adjective_child)+" child, "+fname+" spent most of their early childhood "+childhood_a.text+" and "+childhood_a2.text));

	// Figure out what historical events happened during the characters life time
	var histevents = [];
	var evnames = Object.getOwnPropertyNames(historicalevents);
	for (var i=0; i<evnames.length; i++){
		// evnames are years
		// if year is after birth and affects current location
		var evyear = parseInt(evnames[i]);
		if (evyear > birthyear
		&& historicalevents[evnames[i]].affects.indexOf(location) > -1){
			histevents.push(historicalevents[evnames[i]]);
			// events.push(new Event("historical", evyear, "In "+evyear+" he was in "+location+" when "+historicalevents[evnames[i]].expl));
		}
	}

	console.log("histevents", histevents);
	// Amnesia --> Hide a part of history or rewrite vaguely?
	var wealthdelta = wealth - family_wealth;
	console.log("wealthdelta",wealthdelta);
	if (wealthdelta < 0){
		events.push(new Event("wealth analysis",curyear, fname+"'s life has been going downhill ever since"));
	} else if (wealthdelta > 0){
		events.push(new Event("wealth analysis",curyear, fname+" has been able to secure a decent living for theirself during their life"));
	}



	// Apply received effects(perks, skills...) to character sheet
	for (var i=0; i < effects.length; i++){
		if (effects[i].perk){
			console.log("Applying perk", effects[i].perk);
			// Perks are stored in perks with their cost in the name, and in childhood_activities without the cost.

		}
		if (effects[i].skill){
			console.log("Applying skill", effects[i].skill);

		}
	}


	// Finally serialize events and make story text
	for (var i=0; i<events.length; i++){

		story.push(capitalize(events[i].text));
	}
	story = story.join(". ") + ".";

	console.log(story);
	renderlomake();
	backup();
	getelid("history").innerHTML=story;
}


function an(t){
	// Roughly guesses whether to use a or an (eu, u poses problems)
	t = t.trim(); // get rid of whitespace around edges
	var vowels = "aeio";
	var art = "a";
	if (vowels.indexOf(t[0]) > -1){
		art = "an";
	}
	return art+" "+t;
}

function plural(t) {
	// Pluralizes input word, but only a few special cases are supported. Also doesn't care if word is already plural.
	t = t.trim(); // get rid of whitespace around edges
	if (t.indexOf("man") == t.length-3){
		t = t.substr(0, t.length-2) + 'e' + t.substr(t.length-1);
	} else if (t[t.length-1] == "y"){
		t = t.substr(0, t.length-1) + 'ies';
	} else {
		t = t + "s";
	}
	return t;
}

function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function clamp(val,min,max){
	if (val < min){
		return min;
	} else if (val > max){
		return max;
	} else {
		return val;
	}
}
